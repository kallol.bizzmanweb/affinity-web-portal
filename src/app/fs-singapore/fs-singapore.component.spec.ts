import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsSingaporeComponent } from './fs-singapore.component';

describe('FsSingaporeComponent', () => {
  let component: FsSingaporeComponent;
  let fixture: ComponentFixture<FsSingaporeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsSingaporeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsSingaporeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
