import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplynowResultComponent } from './applynow-result.component';

describe('ApplynowResultComponent', () => {
  let component: ApplynowResultComponent;
  let fixture: ComponentFixture<ApplynowResultComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplynowResultComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplynowResultComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
