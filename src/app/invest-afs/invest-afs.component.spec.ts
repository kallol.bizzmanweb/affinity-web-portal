import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestAfsComponent } from './invest-afs.component';

describe('InvestAfsComponent', () => {
  let component: InvestAfsComponent;
  let fixture: ComponentFixture<InvestAfsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestAfsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestAfsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
