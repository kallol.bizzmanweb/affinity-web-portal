import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';
// import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
// import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  // response;
  // socialusers = new Socialusers();
  loginForm: FormGroup;
  submitted = false;
  loginData;
  isLoggedIn;
  loginError;
  constructor(
    private formBuilder: FormBuilder,
    private userservice: ApiService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  // tslint:disable-next-line: typedef
  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]]
      //  acceptTerms: [false, Validators.requiredTrue]
  });
  }
  // tslint:disable-next-line: typedef
  get f() { return this.loginForm.controls; }

  // tslint:disable-next-line: typedef
  // tslint:disable-next-line: typedef
  onSubmitlogin() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.loginForm.invalid) {
        return;
    }

    // display form values on success
  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value, null, 4));
    this.userservice.login(this.loginForm.value)
      .subscribe(
        data => {
          this.loginData = data;
           if (this.loginData.status === 'error'){
            this.loginError = 'error';
          }
           if (this.loginData.result.auth_token){
             console.log('LOGIN T::::::', this.loginData.result.auth_token);
             // this.router.navigate(['/login', { id: heroId }]);
             localStorage.setItem('isLoggedIn', this.loginData.result.auth_token);
             this.router.navigateByUrl('/userdashboard');
          }else{
            console.log('Undefined LOGIN T::::::', this.loginData.result.auth_token);
            this.router.navigateByUrl('/login');
          }
        //  console.log('LOGIN T::::::', this.loginData.result.auth_token);
          // this.router.navigate(['/login', { id: heroId }]);
        //  localStorage.setItem('isLoggedIn', this.loginData.result.auth_token);
        //  this.router.navigateByUrl('/userdashboard');
        },
        error => {
          console.log('verification error', error);
        });
    }

   // tslint:disable-next-line:typedef
  //  onLogout() {
  //   this.token = null;
  //   localStorage.removeItem('eq_user');
  //   localStorage.clear();
  //   this.authService.logout().subscribe( s => {
  //       this.route.navigate(['login']);
  //  });
  // }
  // onReset() {
  //     this.submitted = false;
  //     this.loginForm.reset();
  // }
   // tslint:disable-next-line:typedef
  //  public socialSignIn(socialProvider: string) {
  //   let socialPlatformProvider;
  //   if (socialProvider === 'facebook') {
  //     socialPlatformProvider = FacebookLoginProvider.PROVIDER_ID;
  //   } else if (socialProvider === 'google') {
  //     socialPlatformProvider = GoogleLoginProvider.PROVIDER_ID;
  //   }

  //   this.OAuth.signIn(socialPlatformProvider).then(socialusers => {
  //     console.log(socialProvider, socialusers);
  //     console.log(socialusers);
  //     this.Savesresponse(socialusers);
  //   });
  // }
  // tslint:disable-next-line:typedef
  // Savesresponse(socialusers: Socialusers) {

  //   this.SocialloginService.Savesresponse(socialusers).subscribe((res: any) => {
  //     // tslint:disable-next-line:no-debugger
  //     debugger;
  //     console.log(res);
  //     this.socialusers = res;
  //     this.response = res.userDetail;
  //     localStorage.setItem('socialusers', JSON.stringify( this.socialusers));
  //     console.log(localStorage.setItem('socialusers', JSON.stringify(this.socialusers)));
  //     this.router.navigate([`/Dashboard`]);
  //   });
  // }

}
