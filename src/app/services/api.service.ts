import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { throwError, BehaviorSubject } from 'rxjs';
import { catchError } from 'rxjs/operators';

// export interface User {
//   fullName: string;
//   email: string;
//   password: string;
//   mobile: string;
// }

@Injectable({
  providedIn: 'root'
})
export class ApiService {
  public isLoggedIn: BehaviorSubject<boolean> = new BehaviorSubject<boolean>(false);
  cors = { 'Access-Control-Allow-Origin': '*' };
  // apiBaseUrl = 'http://localhost:3000/';
  apiBaseUrl = 'http://162.213.248.35:3007/';
  url;
  constructor(private http: HttpClient) { }

  // tslint:disable-next-line: typedef
  register(regObj) {
    return this.http.post(`${this.apiBaseUrl}api/signup`, regObj);
  }
  // tslint:disable-next-line: typedef
  login(loginObj) {
    return this.http.post(`${this.apiBaseUrl}api/login`, loginObj);
  }
  
  forgotpassword(forgotpassObj) {
    return this.http.post(`${this.apiBaseUrl}api/request-forget-password`, forgotpassObj);
  }
  resetpassword(forgotpassObj) {
    return this.http.post(`${this.apiBaseUrl}api/set-new-password`, forgotpassObj);
  }
  // tslint:disable-next-line: typedef
  sendmail(emailObj) {
    return this.http.post(`${this.apiBaseUrl}api/send-mail`, emailObj);
  }
  // tslint:disable-next-line: typedef
  CorpassloginInfo(emailObj) {
    return this.http.post(`${this.apiBaseUrl}getPersonData`, emailObj);
  }
  
  // tslint:disable-next-line:typedef
  SaveCorpassloginData(loginData){
   // console.log('lochan data', loginData);
    return this.http.post(`${this.apiBaseUrl}postPersonData`, loginData);
  }
  // tslint:disable-next-line:typedef
  // isAuthenticated() {
  //  if (localStorage.getItem('isLoggedIn')){
  //     return true;
  //   }
  //   else{
  //     return false;
  //   }
  // }
  // tslint:disable-next-line:typedef
  // Savesresponse(responce)
  // {
  //   this.url =  '${this.apiBaseUrl}/api/Savesresponse';
  //   return this.http.post(this.url, responce);
  // }
}
