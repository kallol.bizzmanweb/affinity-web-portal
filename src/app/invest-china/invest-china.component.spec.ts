import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestChinaComponent } from './invest-china.component';

describe('InvestChinaComponent', () => {
  let component: InvestChinaComponent;
  let fixture: ComponentFixture<InvestChinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestChinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestChinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
