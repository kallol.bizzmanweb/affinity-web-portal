import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatWeDoSingaporeComponent } from './what-we-do-singapore.component';

describe('InvestAfsComponent', () => {
  let component: WhatWeDoSingaporeComponent;
  let fixture: ComponentFixture<WhatWeDoSingaporeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatWeDoSingaporeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatWeDoSingaporeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
