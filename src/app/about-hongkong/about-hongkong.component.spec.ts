import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutHongkongComponent } from './about-hongkong.component';

describe('AboutHongkongComponent', () => {
  let component: AboutHongkongComponent;
  let fixture: ComponentFixture<AboutHongkongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutHongkongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutHongkongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
