import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { HeaderComponent } from './components/header/header.component';
import { FooterComponent } from './components/footer/footer.component';
import { RegistrationComponent } from './registration/registration.component';
//import { LoginComponent } from './login/login.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { InvestAfsComponent } from './invest-afs/invest-afs.component';
import { AboutChinaComponent } from './about-china/about-china.component';
import { AboutSingaporeComponent } from './about-singapore/about-singapore.component';
import { AboutHongkongComponent } from './about-hongkong/about-hongkong.component';
import { AboutAustraliaComponent } from './about-australia/about-australia.component';
import { InvestSingaporeComponent } from './invest-singapore/invest-singapore.component';
import { InvestChinaComponent } from './invest-china/invest-china.component';
import { InvestHongkongComponent } from './invest-hongkong/invest-hongkong.component';
import { InvestAustraliaComponent } from './invest-australia/invest-australia.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';
import { CsrSingaporeComponent } from './csr-singapore/csr-singapore.component';
import { FsSingaporeComponent } from './fs-singapore/fs-singapore.component';
import { FsAustraliaComponent } from './fs-australia/fs-australia.component';
import { ThanksComponent } from './thanks/thanks.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginCorppassComponent } from './login-corppass/login-corppass.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { WhatWeDoChinaComponent } from './what-we-do-china/what-we-do-china.component';
import { WhatWeDoAustraliaComponent } from './what-we-do-australia/what-we-do-australia.component';
import { WhatWeDoHongKongComponent } from './what-we-do-hong-kong/what-we-do-hong-kong.component';
import { WhatWeDoSingaporeComponent } from './what-we-do-singapore/what-we-do-singapore.component';
import { GlobalOffices } from './global-offices/global-offices.component';
import { ApplyNowSingaporeComponent } from './apply-now-singapore/apply-now-singapore.component';
import { AboutusComponents } from './aboutus/aboutus.component';
import { ApplynowSingaporeComponents } from './applynow-singapore/applynow-singapore.component';
import { ApplynowResultComponent } from './applynow-result/applynow-result.component';


// import { GoogleLoginProvider, FacebookLoginProvider, AuthService } from 'angular-6-social-login';
// import { SocialLoginModule, AuthServiceConfig } from 'angular-6-social-login';
// tslint:disable-next-line:typedef
export function socialConfigs() {
  // const config = new AuthServiceConfig(
  //   [
  //     {
  //       id: FacebookLoginProvider.PROVIDER_ID,
  //       provider: new FacebookLoginProvider('app -id')
  //     },
  //     {
  //       id: GoogleLoginProvider.PROVIDER_ID,
  //       provider: new GoogleLoginProvider('app-id')
  //     }
  //   ]
  // );
  // return config;
}

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    HeaderComponent,
    FooterComponent,
    RegistrationComponent,
    //LoginComponent,
    AboutUsComponent,
    ContactUsComponent,
    InvestAfsComponent,
    AboutChinaComponent,
    AboutSingaporeComponent,
    AboutHongkongComponent,
    AboutAustraliaComponent,
    InvestSingaporeComponent,
    InvestChinaComponent,
    InvestHongkongComponent,
    InvestAustraliaComponent,
    WhatWeDoComponent,
    CsrSingaporeComponent,
    FsSingaporeComponent,
    FsAustraliaComponent,
    ThanksComponent,
    DashboardComponent,
    LoginCorppassComponent,
    ForgotPasswordComponent,
    ResetPasswordComponent,
    WhatWeDoChinaComponent,
    WhatWeDoAustraliaComponent,
    WhatWeDoHongKongComponent,
    WhatWeDoSingaporeComponent,
    GlobalOffices,
    ApplyNowSingaporeComponent,
    AboutusComponents,
    ApplynowSingaporeComponents,
    ApplynowResultComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
  //  AuthService,
  //   {
  //     provide: AuthServiceConfig,
  //     useFactory: socialConfigs
  //   }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
