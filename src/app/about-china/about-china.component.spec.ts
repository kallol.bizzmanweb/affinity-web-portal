import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutChinaComponent } from './about-china.component';

describe('AboutChinaComponent', () => {
  let component: AboutChinaComponent;
  let fixture: ComponentFixture<AboutChinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutChinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutChinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
