import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutusComponents } from './aboutus.component';

describe('AboutusComponents', () => {
  let component: AboutusComponents;
  let fixture: ComponentFixture<AboutusComponents>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutusComponents ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutusComponents);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
