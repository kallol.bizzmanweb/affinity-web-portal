import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginCorppassComponent } from './login-corppass.component';

describe('LoginCorppassComponent', () => {
  let component: LoginCorppassComponent;
  let fixture: ComponentFixture<LoginCorppassComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginCorppassComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginCorppassComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
