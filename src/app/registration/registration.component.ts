import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',
  styleUrls: ['./registration.component.css']
})
export class RegistrationComponent implements OnInit {
  registerForm: FormGroup;
  submitted = false;
  registerData;
  constructor(
    private formBuilder: FormBuilder,
    private userservice: ApiService,
    private route: ActivatedRoute,
    private router: Router
    ) { }
  // tslint:disable-next-line: typedef
  ngOnInit() {
    this.registerForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(6)]],
      mobile: ['', Validators.required]
    //  acceptTerms: [false, Validators.requiredTrue]
  });
  }
  // tslint:disable-next-line: typedef
  get f() { return this.registerForm.controls; }

  // tslint:disable-next-line: typedef
  onSubmit() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.registerForm.invalid) {
        return;
    }

    // display form values on success
  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.registerForm.value, null, 4));
    this.userservice.register(this.registerForm.value)
      .subscribe(
        data => {
          this.registerData = data;
           // console.log('subrata new THEN::::::', this.registerData);
          // this.router.navigate(['/login', { id: heroId }]);
          this.router.navigateByUrl('/login');
          },
        error => {
          console.log('verification error', error);
        });
    }
}
