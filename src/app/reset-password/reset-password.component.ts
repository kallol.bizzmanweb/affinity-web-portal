import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-reset-password',
  templateUrl: './reset-password.component.html',
  styleUrls: ['./reset-password.component.css']
})
export class ResetPasswordComponent implements OnInit {

  resetPassword: FormGroup;
  submitted = false;
  resetpasswordData;
  isLoggedIn;
  resetpassError;
  constructor(
    private formBuilder: FormBuilder,
    private userservice: ApiService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
    this.resetPassword = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]],
      password: ['', [Validators.required, Validators.minLength(6)]]
  });
  }
 // tslint:disable-next-line:typedef
 get f() { return this.resetPassword.controls; }
 // tslint:disable-next-line:typedef
 onSubmitResetPass() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.resetPassword.invalid) {
        return;
    }

    // display form values on success
  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value, null, 4));
    this.userservice.resetpassword(this.resetPassword.value)
      .subscribe(
        data => {
          this.resetpasswordData = data;
         // console.log('password data:', this.resetpasswordData);
          if (this.resetpasswordData.status === 'error'){
             this.resetpassError = 'Email Error';
          }
          if (this.resetpasswordData.status === 'success'){
             this.router.navigateByUrl('/login');
          }
          // if (this.passwordData.result.auth_token){
          //    console.log('LOGIN T::::::', this.loginData.result.auth_token);
          //    this.router.navigate(['/login', { id: heroId }]);
          //    localStorage.setItem('isLoggedIn', this.loginData.result.auth_token);
          //    this.router.navigateByUrl('/userdashboard');
          // }
          // else{
          //   console.log('Undefined LOGIN T::::::', this.loginData.result.auth_token);
          //   this.router.navigateByUrl('/login');
          // }
        },
        error => {
          console.log('verification error', error);
        });
    }
}
