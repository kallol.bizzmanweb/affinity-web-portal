import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../../services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
   isLoggedIn: boolean;
   constructor(
    private formBuilder: FormBuilder,
    private userservice: ApiService,
    private route: ActivatedRoute,
    private router: Router
    ) {
       // this.isLoggedIn = this.userservice.isAuthenticated();
        this.userservice.isLoggedIn.subscribe( value => {
            this.isLoggedIn = value;
            if (!this.isLoggedIn){
              this.isLoggedIn = true;
            }
          });
    }


  // tslint:disable-next-line:typedef
  ngOnInit(){
    // this.isLoggedIn = this.userservice.isAuthenticated();
    // this.isLoggedIn = localStorage.getItem('isLoggedIn');
   // console.log('subrata>>', this.isLoggedIn);
  }

// tslint:disable-next-line:typedef
onLogout() {
    localStorage.removeItem('isLoggedIn');
    localStorage.clear();
    this.isLoggedIn = false;
    this.router.navigateByUrl('/home');
  //   this.userservice.logout().subscribe( s => {
  //       this.router.navigateByUrl('/home');
  //  });
  }
}
