import { Component, OnInit } from '@angular/core';
import { GlobalOffices } from '../../global-offices/global-offices.component';
import { Routes, RouterModule } from '@angular/router';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.css']
})
export class FooterComponent implements OnInit {

  constructor() { 
  const routes: Routes = [
    { path: 'global-offices' , component: GlobalOffices }
];
}

  ngOnInit(): void {
  }

}
