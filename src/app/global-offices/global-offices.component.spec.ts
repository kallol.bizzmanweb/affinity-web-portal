import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GlobalOffices } from './global-offices.component';

describe('GlobalOffices', () => {
  let component: GlobalOffices;
  let fixture: ComponentFixture<GlobalOffices>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GlobalOffices ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GlobalOffices);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
