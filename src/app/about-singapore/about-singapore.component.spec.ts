import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutSingaporeComponent } from './about-singapore.component';

describe('AboutSingaporeComponent', () => {
  let component: AboutSingaporeComponent;
  let fixture: ComponentFixture<AboutSingaporeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutSingaporeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutSingaporeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
