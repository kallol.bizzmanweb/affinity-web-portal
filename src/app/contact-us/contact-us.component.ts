import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {
  contactForm: FormGroup;
  submitted = false;
  emailData;
  constructor(
    private formBuilder: FormBuilder,
    private userservice: ApiService,
    private route: ActivatedRoute,
    private router: Router
    ) { }



  // tslint:disable-next-line: typedef
  ngOnInit() {
    this.contactForm = this.formBuilder.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      mobile: ['', [Validators.required, Validators.minLength(10)]],
      msg: ['', Validators.required]
    //  acceptTerms: [false, Validators.requiredTrue]
  });
  }
  // tslint:disable-next-line: typedef
  get f() { return this.contactForm.controls; }

  // tslint:disable-next-line: typedef
  onSubmitContact() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.contactForm.invalid) {
        return;
    }
  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.contactForm.value, null, 4));
    this.userservice.sendmail(this.contactForm.value)
      .subscribe(
        data => {
          this.emailData = data;
          // console.log('jhiku madhupur::::::', this.emailData);
          // this.router.navigate(['/login', { id: heroId }]);
          this.router.navigateByUrl('/thanks');
          },
        error => {
          console.log('verification error', error);
        });
  }
}
