import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestHongkongComponent } from './invest-hongkong.component';

describe('InvestHongkongComponent', () => {
  let component: InvestHongkongComponent;
  let fixture: ComponentFixture<InvestHongkongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestHongkongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestHongkongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
