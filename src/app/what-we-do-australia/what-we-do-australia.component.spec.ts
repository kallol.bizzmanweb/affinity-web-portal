import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatWeDoAustraliaComponent } from './what-we-do-australia.component';

describe('WhatWeDoAustraliaComponent', () => {
  let component: WhatWeDoAustraliaComponent;
  let fixture: ComponentFixture<WhatWeDoAustraliaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatWeDoAustraliaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatWeDoAustraliaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
