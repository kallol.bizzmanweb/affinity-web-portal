import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestAustraliaComponent } from './invest-australia.component';

describe('InvestAustraliaComponent', () => {
  let component: InvestAustraliaComponent;
  let fixture: ComponentFixture<InvestAustraliaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestAustraliaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestAustraliaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
