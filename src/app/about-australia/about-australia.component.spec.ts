import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AboutAustraliaComponent } from './about-australia.component';

describe('AboutAustraliaComponent', () => {
  let component: AboutAustraliaComponent;
  let fixture: ComponentFixture<AboutAustraliaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AboutAustraliaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AboutAustraliaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
