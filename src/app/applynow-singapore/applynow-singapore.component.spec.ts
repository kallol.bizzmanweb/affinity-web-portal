import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplynowSingaporeComponents } from './applynow-singapore.component';

describe('ApplynowSingaporeComponents', () => {
  let component: ApplynowSingaporeComponents;
  let fixture: ComponentFixture<ApplynowSingaporeComponents>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplynowSingaporeComponents ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplynowSingaporeComponents);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
