import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-applynow-singapore',
  templateUrl: './applynow-singapore.component.html',
  styleUrls: ['./applynow-singapore.component.css']
})
export class ApplynowSingaporeComponents implements OnInit {

  constructor(private router: Router) {}

  ngOnInit(): void {
  //this.pdf = 'http://148.66.133.36//applynow';
  }

  action(){
      this.router.navigateByUrl('/applynow');
   }

}
