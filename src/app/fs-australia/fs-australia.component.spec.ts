import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FsAustraliaComponent } from './fs-australia.component';

describe('FsAustraliaComponent', () => {
  let component: FsAustraliaComponent;
  let fixture: ComponentFixture<FsAustraliaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FsAustraliaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FsAustraliaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
