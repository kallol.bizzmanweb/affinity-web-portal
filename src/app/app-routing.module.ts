import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { RegistrationComponent } from './registration/registration.component';
//import { LoginComponent } from './login/login.component';
import { AboutUsComponent } from './about-us/about-us.component';
import { ContactUsComponent } from './contact-us/contact-us.component';
import { InvestAfsComponent } from './invest-afs/invest-afs.component';
import { AboutChinaComponent } from './about-china/about-china.component';
import { AboutSingaporeComponent } from './about-singapore/about-singapore.component';
import { AboutHongkongComponent } from './about-hongkong/about-hongkong.component';
import { AboutAustraliaComponent } from './about-australia/about-australia.component';
import { InvestSingaporeComponent } from './invest-singapore/invest-singapore.component';
import { InvestChinaComponent } from './invest-china/invest-china.component';
import { InvestHongkongComponent } from './invest-hongkong/invest-hongkong.component';
import { InvestAustraliaComponent } from './invest-australia/invest-australia.component';
import { WhatWeDoComponent } from './what-we-do/what-we-do.component';
import { CsrSingaporeComponent } from './csr-singapore/csr-singapore.component';
import { FsSingaporeComponent } from './fs-singapore/fs-singapore.component';
import { FsAustraliaComponent } from './fs-australia/fs-australia.component';
import { ThanksComponent } from './thanks/thanks.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginCorppassComponent } from './login-corppass/login-corppass.component';
import { ForgotPasswordComponent } from './forgot-password/forgot-password.component';
import { ResetPasswordComponent } from './reset-password/reset-password.component';
import { WhatWeDoChinaComponent } from './what-we-do-china/what-we-do-china.component';
import { WhatWeDoAustraliaComponent } from './what-we-do-australia/what-we-do-australia.component';
import { WhatWeDoHongKongComponent } from './what-we-do-hong-kong/what-we-do-hong-kong.component';
import { WhatWeDoSingaporeComponent } from './what-we-do-singapore/what-we-do-singapore.component';
import { GlobalOffices } from './global-offices/global-offices.component';
import { ApplyNowSingaporeComponent } from './apply-now-singapore/apply-now-singapore.component';
import { AboutusComponents } from './aboutus/aboutus.component';
import { ApplynowSingaporeComponents } from './applynow-singapore/applynow-singapore.component';
import { ApplynowResultComponent } from './applynow-result/applynow-result.component';


const routes: Routes = [
  { path: '', redirectTo: '/home', pathMatch: 'full' },
  { path: 'home' , component: HomeComponent },
  { path: 'register' , component: RegistrationComponent },
  //{ path: 'login' , component: LoginComponent },
  { path: 'login-croppass' , component: LoginCorppassComponent },
  { path: 'forgot-password' , component: ForgotPasswordComponent },
  { path: 'reset-password' , component: ResetPasswordComponent },
  { path: 'about-us' , component: AboutUsComponent },
  { path: 'contact-us' , component: ContactUsComponent },
  { path: 'invest-afs' , component: InvestAfsComponent },
  { path: 'about-china' , component: AboutChinaComponent },
  { path: 'about-singapore' , component: AboutSingaporeComponent },
  { path: 'apply-now-singapore' , component: ApplyNowSingaporeComponent },
  { path: 'about-hongkong' , component: AboutHongkongComponent },
  { path: 'about-australia' , component: AboutAustraliaComponent },
  { path: 'invest-china' , component: InvestChinaComponent },
  { path: 'invest-singapore' , component: InvestSingaporeComponent },
  { path: 'invest-hongkong' , component: InvestHongkongComponent },
  { path: 'invest-australia' , component: InvestAustraliaComponent },
  { path: 'what-we-do' , component: WhatWeDoComponent },
  { path: 'csr-singapore' , component: CsrSingaporeComponent },
  { path: 'fs-singapore' , component: FsSingaporeComponent },
  { path: 'fs-australia' , component: FsAustraliaComponent },
  { path: 'global-offices-china' , component: WhatWeDoChinaComponent },
  { path: 'global-offices-australia' , component: WhatWeDoAustraliaComponent },
  { path: 'global-offices-hgk' , component: WhatWeDoHongKongComponent },
  { path: 'global-offices-sgp' , component: WhatWeDoSingaporeComponent },
  { path: 'global-offices' , component: GlobalOffices },
  { path: 'thanks' , component: ThanksComponent },
  { path: 'userdashboard' , component: DashboardComponent },
  { path: 'aboutus' , component: AboutusComponents },
  { path: 'applynow-singapore' , component: ApplynowSingaporeComponents },
  { path: 'applynow-result' , component: ApplynowResultComponent },
 // { path: '**', redirectTo: 'home' , pathMatch: 'full' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
