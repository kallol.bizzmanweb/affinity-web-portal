import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-forgot-password',
  templateUrl: './forgot-password.component.html',
  styleUrls: ['./forgot-password.component.css']
})
export class ForgotPasswordComponent implements OnInit {
  forgotPassword: FormGroup;
  submitted = false;
  passwordData;
  isLoggedIn;
  emailError;
  constructor(
    private formBuilder: FormBuilder,
    private userservice: ApiService,
    private route: ActivatedRoute,
    private router: Router
    ) { }

  // tslint:disable-next-line:typedef
  ngOnInit() {
     this.forgotPassword = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email, Validators.pattern('^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+.[a-zA-Z0-9-.]+$')]]
  });
  }
// tslint:disable-next-line: typedef
  get f() { return this.forgotPassword.controls; }
   // tslint:disable-next-line:typedef
   onSubmitForgotPass() {
    this.submitted = true;
    // stop here if form is invalid
    if (this.forgotPassword.invalid) {
        return;
    }

    // display form values on success
  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value, null, 4));
    this.userservice.forgotpassword(this.forgotPassword.value)
      .subscribe(
        data => {
          this.passwordData = data;
          console.log('password data', this.passwordData);
          if (this.passwordData.status === 'error'){
            this.emailError = 'error';
          }else{
            this.router.navigateByUrl('/login');
          }
          // if (this.passwordData.result.auth_token){
          //    console.log('LOGIN T::::::', this.loginData.result.auth_token);
          //    this.router.navigate(['/login', { id: heroId }]);
          //    localStorage.setItem('isLoggedIn', this.loginData.result.auth_token);
          //    this.router.navigateByUrl('/userdashboard');
          // }
          // else{
          //   console.log('Undefined LOGIN T::::::', this.loginData.result.auth_token);
          //   this.router.navigateByUrl('/login');
          // }
        },
        error => {
          console.log('verification error', error);
        });
    }
}
