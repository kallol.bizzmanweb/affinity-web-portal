import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatWeDoHongKongComponent } from './invest-afs.component';

describe('WhatWeDoHongKongComponent', () => {
  let component: WhatWeDoHongKongComponent;
  let fixture: ComponentFixture<WhatWeDoHongKongComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatWeDoHongKongComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatWeDoHongKongComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
