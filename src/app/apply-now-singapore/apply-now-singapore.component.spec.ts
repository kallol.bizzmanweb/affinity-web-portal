import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ApplyNowSingaporeComponent } from './apply-now-singapore.component';

describe('ApplyNowSingaporeComponent', () => {
  let component: ApplyNowSingaporeComponent;
  let fixture: ComponentFixture<ApplyNowSingaporeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ApplyNowSingaporeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ApplyNowSingaporeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
