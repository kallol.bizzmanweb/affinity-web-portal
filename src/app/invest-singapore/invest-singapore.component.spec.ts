import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { InvestSingaporeComponent } from './invest-singapore.component';

describe('InvestSingaporeComponent', () => {
  let component: InvestSingaporeComponent;
  let fixture: ComponentFixture<InvestSingaporeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ InvestSingaporeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(InvestSingaporeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
