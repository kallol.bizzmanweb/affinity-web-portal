import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, Validators } from '@angular/forms';
import { first } from 'rxjs/operators';
import { ApiService } from '../services/api.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
CorpassLogin: FormGroup;
CorpassLoginData: FormGroup;
submitted = false;
issubmitted = false;
CorpassData;
StoreCorpassData;
constructor(
    private formBuilder: FormBuilder,
    private userservice: ApiService,
    private route: ActivatedRoute,
    private router: Router
    ) { }
isLoggedIn = false;

// tslint:disable-next-line:typedef
ngOnInit() {
   // location.reload();
     // this.isLoggedIn  = this.userservice.isAuthenticated();
    // alert('dash' + this.isLoggedIn);
    // if (this.isLoggedIn){
    //   alert('hello');
    // }
    this.CorpassLogin = this.formBuilder.group({
      email: ['', [Validators.required, Validators.email]]
    });

    this.CorpassLoginData = this.formBuilder.group({
      uinfin: ['', [Validators.required]],
      name: ['', [Validators.required]],
      sex: ['', [Validators.required]],
      race: ['', [Validators.required]],
      nationality: ['', [Validators.required]],
      dob: ['', [Validators.required]],
      email: ['', [Validators.required, Validators.email]],
      mobileno: ['', [Validators.required]],
      regadd: ['', [Validators.required]]
    });
  }
  // tslint:disable-next-line:typedef
  get f() { return this.CorpassLogin.controls; }
// tslint:disable-next-line:typedef
onLoginCorpass(){
  alert('madhupur');
  this.router.navigateByUrl('/login-croppass');
    //   this.submitted = true;
    //   if (this.CorpassLogin.invalid) {
    //     return;
    // }
    // display form values on success
  //  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.loginForm.value, null, 4));
      // this.userservice.CorpassloginInfo(this.CorpassLogin.value)
      // .subscribe(
      //   data => {
      //     this.CorpassData = data;
      //     console.log('LOGIN CORPASS INFO::::::', this.CorpassData);
          // this.router.navigate(['/login', { id: heroId }]);
         // localStorage.setItem('isLoggedIn', this.loginData.result.auth_token);
         // this.router.navigateByUrl('/userdashboard');
        // },
        // error => {
        //   console.log('verification error', error);
        // });
    }

  // tslint:disable-next-line:typedef
  submitCorpassData(){
  this.issubmitted = true;
  // if (this.CorpassLoginData.invalid) {
  //       return;
  //   }
    // display form values on success
  alert('SUCCESS!! :-)\n\n' + JSON.stringify(this.CorpassLoginData.value, null, 4));
  this.userservice.SaveCorpassloginData(this.CorpassLoginData.value)
      .subscribe(
        data => {
          this.StoreCorpassData = data;
          console.log('Save CORPASS Data::::::', this.StoreCorpassData);
          // this.router.navigate(['/login', { id: heroId }]);
         // localStorage.setItem('isLoggedIn', this.loginData.result.auth_token);
         // this.router.navigateByUrl('/userdashboard');
        },
        error => {
          console.log('verification error', error);
        });
  }
}
