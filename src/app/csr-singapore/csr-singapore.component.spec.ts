import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CsrSingaporeComponent } from './csr-singapore.component';

describe('CsrSingaporeComponent', () => {
  let component: CsrSingaporeComponent;
  let fixture: ComponentFixture<CsrSingaporeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CsrSingaporeComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CsrSingaporeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
