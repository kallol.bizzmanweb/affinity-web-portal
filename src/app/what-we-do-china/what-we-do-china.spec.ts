import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WhatWeDoChinaComponent } from './what-we-do-china.component';

describe('WhatWeDoChinaComponent', () => {
  let component: WhatWeDoChinaComponent;
  let fixture: ComponentFixture<WhatWeDoChinaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WhatWeDoChinaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WhatWeDoChinaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
