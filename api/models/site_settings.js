var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var siteSettingsSchema=new Schema({
    settings_name:{type:String,required:true},
    slug:{type:String,required:true},
    setting_value:{type:String},
    instruction:{type:String},
    is_text:{type:Number,default:0},//0=No/1=Yes
    is_textarea:{type:Number,default:0},//0=No/1=Yes
    is_file:{type:Number,default:0},//0=No/1=Yes
    file_type:{type:String},//If file the file_type is mandatory.
    view_order:{type:Number,default:0}
});
module.exports=mongoose.model('site_settings',siteSettingsSchema);