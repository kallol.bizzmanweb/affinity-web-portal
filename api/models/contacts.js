var mongoose=require('mongoose');
var bcrypt=require('bcrypt-nodejs');
var Schema=mongoose.Schema;

var contactSchema=new Schema({
    email:{type:String,required:true,max:255,lowercase:true,trim:true},
    fullName : {type:String,min:6,max:50},
    mobile:{type:String,min:10},
    msg : {type:String,min:10,max:200},
    status:{type:Number,default:1},
    date_of_creation:{type:Number,required:true,default:new Date().getTime()},
    date_of_modification:{type:Number,required:true,default:new Date().getTime()},
    __v : 0
});

module.exports=mongoose.model('contacts',contactSchema);





   