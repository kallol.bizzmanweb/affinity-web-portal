var mongoose=require('mongoose');
var Schema=mongoose.Schema;

var userLogSchema=new Schema({
    _user:{type:Schema.Types.ObjectId,ref:'users',required:true},
    device:{type:String,max:255},
    browser:{type:String,max:255},
    os:{type:String,max:255},
    ip:{type:String,max:255},
    auth_token:{type:String},
    login_time:{type:Number},
    start_activity:{type:Number,default:new Date().getTime()},
    last_activity:{type:Number,default:new Date().getTime()},
});
module.exports=mongoose.model('user_log',userLogSchema);