var mongoose=require('mongoose');
var bcrypt=require('bcrypt-nodejs');
var Schema=mongoose.Schema;

var myinfousersSchema=new Schema({
    uinfin:{type:String,max:50,trim:true},
    name:{type:String,max:50,trim:true},
    sex:{type:String,max:150,trim:true},
    race:{type:String,max:50,trim:true},
    nationality:{type:String,max:50,trim:true},
    dob:{type:String,max:150,trim:true},
    email:{type:String,max:50,trim:true},
    mobileno:{type:String,max:50,trim:true},
    regadd:{type:String,max:150,trim:true},
    status:{type: Number},
    date_of_creation:{type:Number,required:true,default:new Date().getTime()},
    date_of_modification:{type:Number,required:true,default:new Date().getTime()}
   });

module.exports=mongoose.model('myinfousers',myinfousersSchema);





   