exports.SuccessRedirect = function(response, route, message) {

    if(typeof message !== 'undefined') {
	response.redirect(route + '?success=true&message=' + message);
    }
    else {
	response.redirect(route + '?success=true');
    }
};
exports.suffixZero=function(number,digit){
    var s = number+"";
    while (s.length < digit) s = "0" + s;
    return s;
};
exports.call_duration=function(from_milisec,to_milisec){
    var output="00:00";
    if(this.IsNullOrEmpty(from_milisec) || this.IsNullOrEmpty(to_milisec))
        return output;
    if(from_milisec > to_milisec)
    {
        var temp=from_milisec;
        from_milisec=to_milisec;
        to_milisec=temp;
    }
    var timeStart = new Date(from_milisec).getTime();
    var timeEnd = new Date(to_milisec).getTime();
    var hourDiff = timeEnd - timeStart; //in ms
    var secDiff = hourDiff / 1000; //in s
    var minDiff = hourDiff / 60 / 1000; //in minutes
    var hDiff = hourDiff / 3600 / 1000; //in hours
    var humanReadable = {};
    humanReadable.hours = Math.floor(hDiff);
    humanReadable.minutes = minDiff - 60 * humanReadable.hours;
    return this.suffixZero(humanReadable.hours,2)+':'+this.suffixZero(Math.floor(humanReadable.minutes),2);
};
exports.valid_json=function(json){
    if (/^[\],:{}\s]*$/.test(json.replace(/\\["\\\/bfnrtu]/g, '@').
    replace(/"[^"\\\n\r]*"|true|false|null|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?/g, ']').
    replace(/(?:^|:|,)(?:\s*\[)+/g, ''))) {

      return true;

    }else{

      return false;

    }
};
exports.output=function(message,status,result,logout){
    var output={
        'message':message,
        'status':'error',
        'result':'',
    };
    if(status.toLowerCase()=="success")
        output.status="success";
    if(result)
        output.result=result;
    if(logout)
        output.logout=true;
    return output;
};

exports.ErrorRedirect = function(response, route, message) {

    if(typeof message !== 'undefined') {
	response.redirect(route + '?error=true&message=' + message);
    }
    else {
	response.redirect(route + '?error=true');
    }
};

exports.IsNullOrEmpty = function(check){

    var errors = false;

    if( typeof check === 'undefined' ) {
        errors=true;
    }
    else if(!check){
        errors=true;
    }
    return errors;

};
exports.escape=function(text){
    if( this.IsNullOrEmpty(text)) {
        return '';
    }
    var map = {
        '&': '&amp;',
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };

    return text.replace(/[&<>"']/g, function(m) { return map[m]; });
};
exports.unescape=function(string){
    if( this.IsNullOrEmpty(string)) {
        return '';
    }
    var map = {
        '<': '&lt;',
        '>': '&gt;',
        '"': '&quot;',
        "'": '&#039;'
    };
    for (var key in map) {
        var entity = map[key];
        var regex = new RegExp(entity, 'g');
        string = string.replace(regex, key);
    }
    string = string.replace(/&quot;/g, '"');
    string = string.replace(/&#039/g, "'");
    string = string.replace(/&amp;/g, '&');
    return string;
};

exports.Equals = function(one, two) {
    if(one === two)
	return true;
    else
	return false;
};

exports.ValidateEmail = function(email) {
    var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
};
exports.sendMail = function(opta,callback){
    //opta.template = true;
    if(this.IsNullOrEmpty(opta.to))
        return {'status':'error','msg':'Recipient email is required'};
    if(this.IsNullOrEmpty(opta.sub))
        return {'status':'error','msg':'Mail Subject is required'};
    var nodemailer = require('nodemailer');
    var smtpTransport = require('nodemailer-smtp-transport');
    if(this.IsNullOrEmpty(opta.from))
        opta.from = opta.from || constants.smtp_email;
    if(this.IsNullOrEmpty(opta.template))
        opta.template = opta.template || 'TRUE'
    if(opta.template === "TRUE")
    {
        cont='<!DOCTYPE html>\n\
                <html lang="en">\n\
                    <head>\n\
                        <meta charset="UTF-8">\n\
                        <meta http-equiv="Content-type" content="text/html; charset=utf-8" />\n\
                        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />\n\
                        <meta http-equiv="X-UA-Compatible" content="IE=edge" />\n\
                        <meta name="format-detection" content="date=no" />\n\
                        <meta name="format-detection" content="address=no" />\n\
                        <meta name="format-detection" content="telephone=no" />\n\
                        <title>'+SITE_SETTINGS.COMPANY_NAME+'</title>\n\
                        <style type="text/css" media="screen">\n\
                            body { padding:0 !important; margin:0 !important; display:block !important; background:#fbe3d1; -webkit-text-size-adjust:none; }\n\
                            a { color:#ff450a; text-decoration:none; font-size: 13px; line-height: 20px; }\n\
                            p { padding:0 !important; margin:0 !important; font-size: 13px; color: #535353; }\n\
                            td[class="spacer-w"] { padding: 1px; }\n\
                            table[class="action-btn"] { margin-bottom: 10px; }\n\
                            /*responsive*/\n\
                            @media only screen and (max-device-width: 639px), only screen and (max-width: 639px) {\n\
                                table[class="wrapper"] { width: 100% !important; min-width: 100% !important; padding: 10px; }\n\
                                td[class="logo"] { width: 70% !important; min-width: 70% !important; padding: 10px; }\n\
                                td[class="social-link"] { width: 30% !important; min-width: 30% !important; padding: 10px; }\n\
                                td[class="spacer-w"] { width: 100% !important; min-width: 100% !important; padding: 0px; }\n\
                                table[class="action-btn"] { width: 100% !important; min-width: 100% !important; padding: 10px; margin-bottom: 0;}\n\
                            }\n\
                        </style>\n\
                    </head>\n\
                    <body style="padding:0 !important; margin:0 !important; display:block !important; background:#fbe3d1; -webkit-text-size-adjust:none;">\n\
                        <table class="wrapper" width="545" border="0" cellspacing="0" cellpadding="0" align="center" style="font-family: Arial,sans-serif;">\n\
                            <tr>\n\
                                <td>\n\
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">\n\
                                        <tr>\n\
                                            <td height="50"></td>\n\
                                        </tr>\n\
                                    </table>\n\
                                    <!-- HEADER START -->\n\
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">\n\
                                        <tr>\n\
                                            <td class="logo" width="90%" align="left">\n\
	 					<table width="100%" border="0" cellspacing="0" cellpadding="0">\n\
                                                    <tr>\n\
	 						<td><a href="javascript:void(0)"><img src="'+constants.base_url+'uploads/site_settings/'+SITE_SETTINGS.SITE_LOGO+'" alt="logo"></a></td>\n\
                                                    </tr>\n\
	 					</table>\n\
                                            </td>\n\
                                            <td class="social-link" width="10%" align="right">\n\
	 					<table width="100%" border="0" cellspacing="0" cellpadding="0">\n\
                                                    <tr>\n\
	 						<td width="30" align="center"><a href="javascript:void(0)"><img src="'+constants.base_url+'images/facebook.png" alt="facebook"></a></td>\n\
	 						<td width="30" align="center"><a href="javascript:void(0)"><img src="'+constants.base_url+'images/twitter.png" alt="twitter"></a></td>\n\
                                                    </tr>\n\
	 					</table>\n\
                                            </td>\n\
                                        </tr>\n\
                                    </table>\n\
                                    <!-- HEADER End -->\n\
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">\n\
                                        <tr>\n\
                                            <td height="30"></td>\n\
                                        </tr>\n\
                                </table>'+opta.body+'\n\
                                <table width="100%" border="0" cellspacing="0" cellpadding="0" align="center">\n\
                                    <tr>\n\
	 				<td height="50"></td>\n\
                                    </tr>\n\
                                </table>\n\
                            </td>\n\
                        </tr>\n\
                    </table>\n\
                </body>\n\
            </html>\n\
                        ';
    }
    else
    {
        cont=opta.body;
    }
    var transporter = nodemailer.createTransport(smtpTransport({
        host: constants.smtp_host,
        port: constants.smtp_port,
        secure: true,
        auth: {
            user: constants.smtp_user,
            pass: constants.smtp_password
        }
    }));
    var mailOptions = {
        from: '"'+SITE_SETTINGS.SITE_NAME+' " <'+opta.from+'>', // sender address
        to: opta.to, // list of receivers
        subject: opta.sub, // Subject line
        html: cont // html body
    };
    transporter.sendMail(mailOptions, function(error, info){
        callback(error,info);
    });
};

exports.ValidateDate = function(dateString) {
    // Check pattern
    if(!/^\d{4}\/\d{1,2}\/\d{1,2}$/.test(dateString))
        return false;

    // Parse the date parts to integers
    var parts = dateString.split("/");
    var year = parseInt(parts[0], 10);
    var month = parseInt(parts[1], 10);
    var day = parseInt(parts[2], 10);

    if(year < 1000 || year > 3000 || month === 0 || month > 12)
        return false;

    var monthLength = [ 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 ];

    // Adjust for leap years
    if(year % 400 === 0 || (year % 100 !== 0 && year % 4 === 0))
        monthLength[1] = 29;

    return day > 0 && day <= monthLength[month - 1];
};


exports.outputSpl=function(message,totnum,status,result){
    var output={
        'message':message,
        'status':'error',
        'totalNumber':totnum,
        'PROFILE_IMAGE_PATH':constants.profile_image_public_link,
        'GROUP_IMAGE_PATH':constants.group_image_public_link,
        'result':''
    };
    if(status.toLowerCase()=="success")
        output.status="success";
    if(result)
        output.result=result;
    return output;
};
