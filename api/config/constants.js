var fs = require("fs");
module.exports={
    'enviroment':process.env.CVMAKER_APP_ENV,
    'server_port':process.env.CVMAKER_HTTP_PORT,
    'ssl_server_port':process.env.CVMAKER_HTTPS_PORT,
    'base_url':process.env.CVMAKER_BASE_URL,
    'file_storage':process.env.CVMAKER_STORAGE_DIR,
    'auth_server':process.env.CVMAKER_AUTHSERVER_URL || 'http://localhost:3001/authentication',//'http://localhost:3002/authentication',
    'api_base_url':this.base_url+'api_v1',
    'api_route_path':'/api_v1/',
    'database':process.env.CVMAKER_DATABASE,
    'secret_key':process.env.CVMAKER_SECRETE_KEY,
    'issuer':process.env.CVMAKER_ISSUER,
    'audience':process.env.CVMAKER_AUDIENCE,
    'algorithms':process.env.CVMAKER_ALGORITHMS,
    'expireToken':false,
    'facebook_client_id':process.env.CVMAKER_FACEBOOK_CLIENT_ID || '498067767417400',
    'facebook_client_secret':process.env.CVMAKER_FACEBOOK_CLIENT_SECRET || 'cc4face6551dc5e104cc1bd0e33f3b65',
    'google_client_id':process.env.CVMAKER_GOOGLE_CLIENT_ID,
    'google_client_secret':process.env.CVMAKER_GOOGLE_CLIENT_SECRET,
    'linkedin_consumer_key':process.env.CVMAKER_LINKEDIN_CONSUMER_KEY,
    'linkedin_consumer_secret':process.env.CVMAKER_LINKEDIN_CONSUMER_SECRET,
    'profile_image_path':'../NodeJS/public/uploads/profile_images',
    'group_image_path':'../NodeJS/public/uploads/group_images',
    'contact_book_image_path':'../NodeJS/public/uploads/contact_book_images',
    'profile_image_public_link':process.env.CVMAKER_BASE_URL+'uploads/profile_images/',
    'group_image_public_link':process.env.CVMAKER_BASE_URL+'/uploads/group_images/',
    'contact_book_image_public_link':process.env.CVMAKER_BASE_URL+'/uploads/contact_book_images/',
    /*
    'smtp_email':'webmaster@CVMAKER.com',
    'smtp_host':'mailtrap.io',
    'smtp_port':2525,
    'smtp_user':'a75e152cb1756a',
    'smtp_password': '4c33a362faba7b',
    */

    'smtp_email':process.env.CVMAKER_SMTP_EMAIL,
    'smtp_host':process.env.CVMAKER_SMTP_HOST,
    'smtp_port':process.env.CVMAKER_SMTP_PORT,
    'smtp_user':process.env.CVMAKER_SMTP_USER,
    'smtp_password': process.env.CVMAKER_SMTP_PASSWORD,
    'max_record_per_page':10
    // 'getPrivateKey':function(){
    //     return fs.readFileSync('./keys/private.pem');
    // },
    // 'getPublicKey':function(){
    //     return fs.readFileSync('./keys/public.pem');
    // },
    // 'getDomainSSLKey':function(){
    //     return fs.readFileSync('./ssl/private.key');
    // },
    // 'getDomainSSLCertificate':function(){
    //     return fs.readFileSync('./ssl/CVMAKER-stage_com.crt');
    // },
    // 'getDomainRootChainCertificate':function(){
    //     return temp=[
    //         fs.readFileSync('./ssl/AddTrustExternalCARoot.crt'),
    //         fs.readFileSync('./ssl/COMODORSAAddTrustCA.crt'),
    //         fs.readFileSync('./ssl/COMODORSADomainValidationSecureServerCA.crt'),
    //     ];
    // }
};