var User = require('../models/users');
var JwtStrategy = require('passport-jwt').Strategy;
var ExtractJwt = require('passport-jwt').ExtractJwt;
var FacebookTokenStrategy = require('passport-facebook-token');
var GoogleTokenStrategy = require('passport-google-plus-token');
//var LinkedinTokenStrategy = require('passport-linkedin-token-oauth2').Strategy;

module.exports = function (passport) {
  var opts = {}
  opts.jwtFromRequest = ExtractJwt.fromAuthHeader();
  opts.secretOrKey = constants.getPublicKey();
  opts.issuer = constants.issuer;
  opts.audience = constants.audience;
  opts.algorithms = constants.algorithms;
  opts.passReqToCallback = true;
  passport.use(new JwtStrategy(opts, function (req, jwt_payload, done) {
    if (!jwt_payload.facebook_id && !jwt_payload.google_id) {
      User.findOne({ _id: jwt_payload._id }, function (err, user) {
        if (err) {
          done(err, false);
        }
        if (user) {
          req.user = user;
          done(null, user);
        } else {
          done(null, false);
        }
      });
    }
    if (jwt_payload.facebook_id && !jwt_payload.google_id) {
      console.log('passport');
      passport.authenticate('facebook-token', { session: false }, function (err, user, info) {
        if (!Utility.IsNullOrEmpty(err))
          if (!Utility.IsNullOrEmpty(err.oauthError))
            done(null, false);
        if (user) {
          req.user = user;
          done(null, user);
        } else {
          done(null, false);
        }
      })(req)
    }

    if (!jwt_payload.facebook_id && jwt_payload.google_id) {
      passport.authenticate('google-token', { session: false }, function (err, user, info) {
        if (!Utility.IsNullOrEmpty(err))
          if (!Utility.IsNullOrEmpty(err.name))
            if (err.name === "InternalOAuthError")
              done(null, false);
        if (user) {
          req.user = user;
          done(null, user);
        } else {
          done(null, false);
        }
      })(req)
    }
  }));

  passport.use(new FacebookTokenStrategy({
    clientID: constants.facebook_client_id,
    clientSecret: constants.facebook_client_secret,
  },
    function (accessToken, refreshToken, profile, done) {
      User.findOne({ facebook_id: profile.id, 'status': { $ne: 3 } }, function (err, user) {
        if (err) {
          return done(err, false);
        }
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      });
    }
  ));

  passport.use(new GoogleTokenStrategy({
    clientID: constants.google_client_id,
    clientSecret: constants.google_client_secret,
  },
    function (accessToken, refreshToken, profile, done) {
      User.findOne({ google_id: profile.id, 'status': { $ne: 3 } }, function (err, user) {
        if (err) {
          return done(err, false);
        }
        if (user) {
          done(null, user);
        } else {
          done(null, false);
        }
      });
    }
  ));

  // passport.use(new LinkedinTokenStrategy({
  //   clientID: constants.linkedin_consumer_key,
  //   clientSecret: constants.linkedin_consumer_secret,
  // },
  //   function (accessToken, refreshToken, profile, done) {
  //     User.findOne({ linkedin_id: profile.id, 'status': { $ne: 3 } }, function (err, user) {
  //       if (err) {
  //         return done(err, false);
  //       }
  //       if (user) {
  //         done(null, user);
  //       } else {
  //         done(null, false);
  //       }
  //     });
  //   }
  // ));
};