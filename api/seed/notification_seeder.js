var Notification = require('../models/chat_notifies');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/yuvitime');
var notifications = [
    new Notification({
        _user: 'Site Name',
        notification_message:'site_name',
        unread:'YuviTime',
        is_notified:false,
        redirect_rul:'instragram_url'
      }),
    new Notification({
        _user: 'Company Name',
        notification_message:'company_name',
        unread:'YuviTime',
        is_notified:false,
        redirect_rul:'instragram_url'
     }),
    new Notification({
        _user: 'Company Description',
        notification_message:'company_description',
        unread:'YuviTime',
        is_notified:false,
        redirect_rul:new Date().getTime(),
        date_of_creation:1,
    
    }),
    new Notification({
        _user: 'Company Email',
        notification_message:'company_email',
        unread:'webmaster@yuvitime.com',
        is_notified:false,
        redirect_rul:'instragram_url'
     
        
    }),
    new Notification({
        _user: 'Admin Email',
        notification_message:'admin_email',
        unread:'support.fdx@gmail.com',
        is_notified:false,
        redirect_rul:'instragram_url'
        
        
        
        
    }),
    new Notification({
        _user: 'Site Logo',
        notification_message:'site_logo',
        unread:'logo.png',
        is_notified:false,
        redirect_rul:new Date().getTime(),
        
        is_file:1,
        file_type:'.png',
        
    }),
    new Notification({
        _user: 'Site Logo',
        notification_message:'site_logo',
        unread:'logo.png',
        is_notified:false,
        redirect_rul:new Date().getTime(),
        
        is_file:1,
        file_type:'.png',
        
    }),
    new Notification({
        _user: 'Email Caption',
        notification_message:'email_caption',
        unread:'Yuvitme.com',
        is_notified:false,
        redirect_rul:'instragram_url'
        
        
        
        
    }),
    new Notification({
        _user: 'Site Meta Keyword',
        notification_message:'site_meta_keyword',
        unread:false,
        is_notified:false,
        redirect_rul:new Date().getTime(),
        date_of_creation:1,
        
        
        
    }),
    new Notification({
        _user: 'Site Meta Description',
        notification_message:'site_meta_description',
        unread:false,
        is_notified:false,
        redirect_rul:new Date().getTime(),
        date_of_creation:1,
    }),
    new Notification({
        _user: 'No User Image',
        notification_message:'no_user_image',
        unread:'147548651785.jpg',
        is_notified:false,
        redirect_rul:new Date().getTime(),
        
       
        
    }),
    new Notification({
        _user: 'Site Contact',
        notification_message:'site_contact',
        unread:'+1-xxxxxxxxxx',
        is_notified:false,
        redirect_rul:'instragram_url'
        
    }),
    new Notification({
        _user: 'Site Address',
        notification_message:'site_address',
        unread:'',
        is_notified:false,
        redirect_rul:new Date().getTime(),
        date_of_creation:1,
   }),
    new Notification({
        _user: 'Admin Name',
        notification_message:'admin_name',
        unread:'Vikram Kumar',
        is_notified:false,
        redirect_rul:'instragram_url'
        
   }),
    new Notification({
        _user: 'Admin Image',
        notification_message:'admin_image',
        unread:'',
        is_notified:false,
        redirect_rul:new Date().getTime(),
        
        is_file:1,
    }),
    new Notification({
        _user: 'Facebook URL',
        notification_message:'any sort of message',
        unread:'',
        is_notified:false,
        redirect_rul:'instragram_url'
        
    }),
    new Notification({
        _user: 'Google URL',
        notification_message:'google_url',
        unread:'',
        is_notified:false,
        redirect_rul:'instragram_url'
        
    }),
    new Notification({
        _user: 'Twitter URL',
        notification_message:'any sort of message',
        unread:'',
        is_notified:false,
        redirect_rul:'instragram_url'
        
    }),
    new Notification({
        _user: 'Linkedin URL',
        notification_message:'any sort of message',
        unread:'',
        is_notified:false,
        redirect_rul:'instragram_url'
        
     }),
    new Notification({
        _user: 'Instragram URL',
        notification_message:'instragram_url',
        unread:'',
        is_notified:false,
        redirect_rul:'instragram_url'
        
    }),
    new Notification({
        _user: 'Youtube URL',
        notification_message:'any sort of message',
        unread:'',
        is_notified:false,
        redirect_rul:'instragram_url'
        
    }),
    new Notification({
        _user: 'Vimeo URL',
        notification_message:'any sort of message',
        unread:'',
        is_notified:false,
        redirect_rul:'instragram_url'
        
    }),
];
var countx=0;
for(var i=0;i< notifications.length;i++){
    notifications[i].save(function(err,result){
        countx++;
        if(countx == notifications.length)
        {
            mongoose.disconnect();
        }
    });
}