var SiteSetting = require('../models/site_settings');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/yuvitime');
var siteSettings = [
    new SiteSetting({
        settings_name: 'Site Name',
        slug:'site_name',
        setting_value:'YuviTime',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Company Name',
        slug:'company_name',
        setting_value:'YuviTime',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Company Description',
        slug:'company_description',
        setting_value:'YuviTime',
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:1,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Company Email',
        slug:'company_email',
        setting_value:'webmaster@yuvitime.com',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Admin Email',
        slug:'admin_email',
        setting_value:'support.fdx@gmail.com',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Site Logo',
        slug:'site_logo',
        setting_value:'logo.png',
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:1,//0=No/1=Yes
        file_type:'.png',//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Site Logo',
        slug:'site_logo',
        setting_value:'logo.png',
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:1,//0=No/1=Yes
        file_type:'.png',//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Email Caption',
        slug:'email_caption',
        setting_value:'Yuvitme.com',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Site Meta Keyword',
        slug:'site_meta_keyword',
        setting_value:null,
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:1,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Site Meta Description',
        slug:'site_meta_description',
        setting_value:null,
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:1,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'No User Image',
        slug:'no_user_image',
        setting_value:'147548651785.jpg',
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:1,//0=No/1=Yes
        file_type:'.jpg',//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Site Contact',
        slug:'site_contact',
        setting_value:'+1-xxxxxxxxxx',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Site Address',
        slug:'site_address',
        setting_value:'',
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:1,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Admin Name',
        slug:'admin_name',
        setting_value:'Vikram Kumar',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Admin Image',
        slug:'admin_image',
        setting_value:'',
        instruction:null,
        is_text:0,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:1,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Facebook URL',
        slug:'facebook_url',
        setting_value:'',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Google URL',
        slug:'google_url',
        setting_value:'',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Twitter URL',
        slug:'Twitter_url',
        setting_value:'',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Linkedin URL',
        slug:'linkedin_url',
        setting_value:'',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Instragram URL',
        slug:'instragram_url',
        setting_value:'',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Youtube URL',
        slug:'youtube_url',
        setting_value:'',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
    new SiteSetting({
        settings_name: 'Vimeo URL',
        slug:'vimeo_url',
        setting_value:'',
        instruction:null,
        is_text:1,//0=No/1=Yes
        is_textarea:0,//0=No/1=Yes
        is_file:0,//0=No/1=Yes
        file_type:null,//If file the file_type is mandatory.
        view_order:0
    }),
];
var countx=0;
for(var i=0;i<siteSettings.length;i++){
    siteSettings[i].save(function(err,result){
        countx++;
        if(countx == siteSettings.length)
        {
            mongoose.disconnect();
        }
    });
}