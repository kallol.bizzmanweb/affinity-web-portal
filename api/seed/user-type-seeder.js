var UserType = require('../models/user_types');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/yuvitime');
var userTypes = [
    new UserType({
        type:'Admin',
        slug:'admin',
    }),
    new UserType({
        type:'End User',
        slug:'end-user',
    }),
];
var countx=0;
for(var i=0;i<userTypes.length;i++){
    userTypes[i].save(function(err,result){
        countx++;
        if(countx == userTypes.length)
        {
            mongoose.disconnect();
        }
    });
}