var Group = require('../models/group');
var mongoose = require('mongoose');
mongoose.connect('mongodb://localhost/yuvitime');
var groups = [
    new Group({
        'name':'Development Zone',
    }),
    new Group({
        'name':'Yuvitime Internal Meeting',
    }),
];
var countx=0;
for(var i=0;i<groups.length;i++){
    groups[i].save(function(err,result){
        countx++;
        if(countx == groups.length)
        {
            mongoose.disconnect();
        }
    });
}
/*
 * new CallHistory({
        'session_id':'6932d496-4b5d-435e-88dd-90457b52a9e6',
        '_caller':'',
        '_callee':'',
        'call_type':1,
        '_group':null,
        'call_initiate_time':'',
        'call_last_activity':'',
        'have_seen':0,
        'status':2
    }),
    new CallHistory({
        'session_id':'331930b6-d78f-46f3-b38b-9d0ca39c3cf4',
        '_caller':'',
        '_callee':'',
        'call_type':1,
        '_group':null,
        'call_initiate_time':'',
        'call_last_activity':'',
        'have_seen':0,
        'status':3
    }),
    new CallHistory({
        'session_id':'e53fc44e-6add-4c6a-affd-99cb75d7d219',
        '_caller':'',
        '_callee':'',
        'call_type':1,
        '_group':'',
        'call_initiate_time':'',
        'call_last_activity':'',
        'have_seen':0,
        'status':1
    }),
    new CallHistory({
        'session_id':'13fb16e2-aa01-4c64-8d52-82f90104b169',
        '_caller':'',
        '_callee':'',
        'call_type':1,
        '_group':'',
        'call_initiate_time':'',
        'call_last_activity':'',
        'have_seen':0,
        'status':2
    }),
    new CallHistory({
        'session_id':'10df3747-87f1-4efc-a20c-ac3b383505d3',
        '_caller':'',
        '_callee':'',
        'call_type':1,
        '_group':'',
        'call_initiate_time':'',
        'call_last_activity':'',
        'have_seen':0,
        'status':3
    }),
 */