var express = require('express');
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  res.send('respond with tasts cv maker ');
});
var UserController=new require('../controllers/UserController');
var ProfileController=new require('../controllers/ProfileController');
//var passport=require('passport');
//require('../config/passport')(passport);
var UserControllerInstance = new UserController();
var ProfileControllerInstance = new ProfileController();
/*
router.get('/signup',notLoggedIn,function(req,res,next){
    var messages=req.flash('error');
    res.render('user/signup',{ title:'Sign UP',csrfToken:req.csrfToken(),messages:messages,hasError:messages.length>0});
});
*/
router.post('/auth/signup',function(req,res,next){
       UserControllerInstance.signup(req,res,next);
});
router.get('/auth/facebook_signup',function(req,res,next){
    UserControllerInstance.facebook_signup(req,res,next);
});
router.post('/auth/login',function(req,res,next){
    UserControllerInstance.login(req,res,next);
});
router.post('/list_users',function(req,res,next){
   UserControllerInstance.list_users(req,res,next);
});
router.post('/add_employee',function(req,res,next){
    UserControllerInstance.add_employee(req,res,next);
});
router.post('/get_employee_by_id',function(req,res,next){
    UserControllerInstance.get_employee_by_id(req,res,next);
});
router.post('/edit_employee_by_id',function(req,res,next){
    UserControllerInstance.edit_employee_by_id(req,res,next);
});

router.post('/logout',function(req,res,next){
    UserControllerInstance.logout(req,res,next);
});

router.post('/request_forget_password',function(req,res,next){
    UserControllerInstance.request_forget_password(req,res,next);
});
router.post('/verify_forget_password_token',function(req,res,next){
    UserControllerInstance.verify_forget_password_token(req,res,next);
});
router.post('/set_new_password',function(req,res,next){
    UserControllerInstance.set_new_password(req,res,next);
});
router.post('/verification',function(req,res,next){
    UserControllerInstance.email_verification(req,res,next);
});
router.post('/resend_verification',function(req,res,next){
    UserControllerInstance.resend_email_verification(req,res,next);
});
router.post('/get-profile',function(req,res,next){
     ProfileControllerInstance.get_profile(req,res,next);
});
router.post('/update-profile',function(req,res,next){
     ProfileControllerInstance.update_profile(req,res,next);
});
router.post('/add-profile',function(req,res,next){
     ProfileControllerInstance.add_profile(req,res,next);
});

module.exports = router;
