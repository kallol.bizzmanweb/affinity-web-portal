var express = require('express');
var router = express.Router();
var UserController=new require('../controllers/UserController');
var UpgradeController=new require('../controllers/UpgradeController');
// var passport=require('passport');
// require('../config/passport')(passport);
var UserControllerInstance=new UserController();
var UpgradeControllerInstance=new UpgradeController();

router.post('/upgrade-pro',function(req,res,next){
      UpgradeControllerInstance.upgrade_pro(req,res,next);
});
router.post('/get-price',function(req,res,next){

      UpgradeControllerInstance.get_price(req,res,next);
});
// router.post('/get-resume',function(req,res,next){
//       TemplateControllerInstance.get_resume(req,res,next);
// });
// router.post('/update-resume',function(req,res,next){
//       TemplateControllerInstance.update_resume(req,res,next);
// });

module.exports = router;
