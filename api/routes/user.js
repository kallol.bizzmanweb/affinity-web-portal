var express = require('express');
var router = express.Router();
var UserController=new require('../controllers/UserController');
var passport=require('passport');
//require('../config/passport')(passport);
var UserControllerInstance=new UserController();

router.post('/signup',function(req,res,next){
    UserControllerInstance.signup(req,res,next);
});
router.post('/facebook_signup',function(req,res,next){
    UserControllerInstance.facebook_signup(req,res,next);
});
router.post('/google_signup',function(req,res,next){
    UserControllerInstance.google_signup(req,res,next);
});

router.post('/login',function(req,res,next){
    UserControllerInstance.login(req,res,next);
});
router.post('/logout',function(req,res,next){
    UserControllerInstance.logout(req,res,next);
});
router.post('/facebook_login',function(req,res,next){
    UserControllerInstance.facebook_login(req,res,next);
});
router.post('/google_login',function(req,res,next){
    UserControllerInstance.google_login(req,res,next);
});
router.post('/request-forget-password',function(req,res,next){
    console.log('in routes',req.body);
    UserControllerInstance.request_forget_password(req,res,next);
});
router.post('/verify_forget_password_token',function(req,res,next){
    UserControllerInstance.verify_forget_password_token(req,res,next);
});
router.post('/set-new-password',function(req,res,next){
    UserControllerInstance.set_new_password(req,res,next);
});
router.post('/email-verification',function(req,res,next){
    UserControllerInstance.email_verification(req,res,next);
});
router.post('/resend-verification',function(req,res,next){
    UserControllerInstance.resend_email_verification(req,res,next);
});
router.post('/send-mail',function(req,res,next){
    UserControllerInstance.send_mail(req,res,next);
});
router.post('/profile',isLoggedIn,function(req,res,next){
     return res.json(Utility.output('Profile','SUCCESS',req.user));
    //isLoggedIn(req,res,next);
});

module.exports = router;
function isLoggedIn(req,res,next){
    passport.authenticate('jwt', { session: false},function(err, user, info){
        if(!Utility.IsNullOrEmpty(info))
            if(info.name === 'JsonWebTokenError')
               return res.json(Utility.output('Invalid Token','ERROR'));
        if(!user)
            return res.json(Utility.output('Not logged in','ERROR'));
        next();
    })(req,res,next);
}
//{ [JsonWebTokenError: invalid token] name: 'JsonWebTokenError', message: 'invalid token' }

