var express = require('express');
var router = express.Router();
var UserController=new require('../controllers/UserController');
var MyinfoController=new require('../controllers/MyinfoController');
var UserControllerInstance=new UserController();
var MyinfoControllerInstance=new MyinfoController();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: 'Express' });
});

router.post('/getPersonData',function(req,res,next){
      MyinfoControllerInstance.getPersonData(req,res,next);
});
router.post('/postPersonData',function(req,res,next){
      MyinfoControllerInstance.postPersonData(req,res,next);
});
module.exports = router;
