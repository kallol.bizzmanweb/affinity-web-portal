var express = require('express');
var router = express.Router();
var UserController=new require('../controllers/UserController');
var PaymentController=new require('../controllers/PaymentController');
// var passport=require('passport');
// require('../config/passport')(passport);
var UserControllerInstance=new UserController();
var PaymentControllerInstance=new PaymentController();

router.get('/buy',function(req,res,next){
    PaymentControllerInstance.buy(req,res,next);
});
router.get('/success',function(req,res,next){
    PaymentControllerInstance.success(req,res,next);
});
router.get('/err' , function(req,res,next) {
      PaymentControllerInstance.err(req,res,next);
});

module.exports = router;
