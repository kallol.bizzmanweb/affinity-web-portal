var UserModel=require('../models/users');
var UsertypeModel = require('../models/nuser_types');
var mongoose = require('mongoose');
module.exports=function usertypeController(){
    /**
    * <h1>[API] Get usertype Information</h1>
    * <p>This api use for listing of usertypes</p>
    * @see API_BASE_URL+pms/list_usertype</a>
    * @header Authorization: Auth Token [*Mandatory][String]
    * @header Content-Type: application/x-www-form-urlencoded
    * <pre>
    * {@code
    *   [
          {
            "_id": "5b8f9b6b4ee90125c8a448a9",
            "status": "Active",
            "usertype": "4545",
            "__v": 0,
            "date_of_modification": 1536138091369,
            "date_of_creation": 1536138091369
        }
      ]
    * </pre>
    * @author Subrata Nandi
    */
    this.list_usertype=function(req,res,next){
        UsertypeModel.find({},function(err,allusertypes){
            if(err)
               return res.json(Utility.output(Toster.FETCH_INFO_ERROR,'ERROR')); 
            if(!allusertypes)
              return res.json(Utility.output(Toster.FETCH_INFO_ERROR,'ERROR')); 
            if(allusertypes)
              {
                allusertypes.forEach(function(key,value){
                   if(allusertypes[value].status == 1){
                    allusertypes[value].status = "Active"
                    }
                    if(allusertypes[value].status == 0){
                        allusertypes[value].status = "Inactive"
                    }
                 });
              }
          return res.json(Utility.output(Toster.LIST_USERTYPES, 'SUCCESS', allusertypes));
         //  res.status(200).send(allusertypes);
        });
    };
    
    
    /**
    * <h1>[API] Upload Profile Image</h1>
    * <p>This api use for adding usertype</p>
    * @param {File} file [*Mandatory]
    * @see API_BASE_URL+pms/add_usertype</a>
    * @header Authorization: Auth Token [*Mandatory][String]
    * <pre>
    * {@code
    *    {
    *      "message": "usertype added Successfully",
    *      "status": "success",
    *      "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata Nandi
    */
   
    this.add_usertype=function(req,res,next){
       var currentTime =new Date().getTime();
       req.assert('usertype','User type is required').notEmpty();
       req.assert('status','Status is required').notEmpty();
     
       var errors = req.validationErrors();
       if (errors) {
           var messages = [];
           errors.forEach(function (error) {
               messages.push(error.msg);
           });
           return res.json(Utility.output(messages, 'ERROR'));
       }
       var  newUsertype = new UsertypeModel();
            newUsertype.usertype = req.body.usertype;
            newUsertype.status = req.body.status;
            newUsertype.date_of_creation = currentTime;
            newUsertype.date_of_modification = currentTime;
            newUsertype.save(function(err,result){
               if(err){
                return res.json(Utility.output(Toster.FETCH_INFO_ERROR,'ERROR')); 
               }
               return res.json(Utility.output(Toster.USERTYPES_ADDED, 'SUCCESS', result));
             })
     };
   
     /**
    * <h1>[API] Get usertype Infomation</h1>
    * <p>This api use to get information for edit purpose</p>
    * @param NULL
    * @see API_BASE_URL+pms/get_usertype</a>
    * @header Content-Type: application/x-www-form-urlencoded
    * @header Authorization: Auth Token [*Mandatory][String]
    * <pre>
    * {@code
    *    {
    *      "message": "usertype fetched successfully",
    *      "status": "success",
    *      "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata Nandi
    */
    this.get_usertype=function(req,res,next){
        req.assert('_id','_id is required').notEmpty();
        var errors=req.validationErrors();
        if(errors){
            var messages=[];
            errors.forEach(function(error){
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages,'ERROR'));
        }
        UsertypeModel.findOne({'_id':req.body._id},function(err,usertype){
        if(err)
              {
                return res.json(Utility.output(Toster.FETCH_INFO_ERROR,'ERROR')); 
              }
              return res.json(Utility.output(Toster.GET_USERTYPES_BY_ID, 'SUCCESS', usertype));
         });
    };

    
  
    /**
    * <h1>[API] Edit Profile Infomation</h1>
    * <p>This api use for change/edit usertype set</p>
    * @param NULL
    * @see API_BASE_URL+pms/edit_usertype</a>
    * @header Content-Type: application/x-www-form-urlencoded
    * @header Authorization: Auth Token [*Mandatory][String]
    * <pre>
    * {@code
    *    {
    *      "message": "usertype edited successfully",
    *      "status": "success",
    *      "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata Nandi
    */
   
    
    this.edit_usertype=function(req,res,next){
        var currentTime =new Date().getTime();
        req.assert('usertype','usertype is required').notEmpty();
        req.assert('status','Status is required').notEmpty();
        req.assert('_id','id is required').notEmpty();
        var errors=req.validationErrors();
        if(errors){
            var messages=[];
            errors.forEach(function(error){
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages,'ERROR'));
        }
       
        UsertypeModel.findOne({_id: req.body._id}, function (err, usertype) {
            if (usertype)
            {
                var usertypeObj = {};
                var usertypeObj = {
                    usertype: req.body.usertype,
                    status: req.body.status,
                    _id: req.body._id,
                    date_of_creation : currentTime,
                    date_of_modification : currentTime
                };
              
                UsertypeModel.update({_id: req.body._id},{$set:usertypeObj}).exec(function (err, result) {
                    if (err) {
                      return res.json(Utility.output(Toster.FETCH_INFO_ERROR,'ERROR')); 
                    }
                     return res.json(Utility.output(Toster.EDIT_USERTYPES_BY_ID, 'SUCCESS', result));
                });
             }
        });
    };
    
    /**
    * <h1>[API] Delete usertype against id</h1>
    * <p>This api use for change/edit profile infomation</p>
    * @param {String} _id [*Mandatory]
    * @see API_BASE_URL+pms/delete_usertype</a>
    * @header Content-Type: application/x-www-form-urlencoded
    * @header Authorization: Auth Token [*Mandatory][String]
    * <pre>
    * {@code
    *    {
    *      "message": "usertype deleted successfully",
    *      "status": "success",
    *      "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata Nandi
    */
    this.delete_usertype=function(req,res,next){
        req.assert('_id','_id is required').notEmpty();
        var errors=req.validationErrors();
        if(errors){
            var messages=[];
            errors.forEach(function(error){
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages,'ERROR'));
        }
        var usertypeObj = {
            _id: req.body._id,
            msg: "Document is deleted against id:"+ req.body._id,
            status: "success"
        };
        UsertypeModel.find({'_id':req.body._id},function(err,usertype){
        if(err){
                return res.json(Utility.output(Toster.FETCH_INFO_ERROR,'ERROR')); 
            }
            UsertypeModel.remove({_id: req.body._id}, function(err){
                if(err) res.json(err);
                return res.json(Utility.output(Toster.DELETE_USERTYPES_BY_ID, 'SUCCESS', usertypeObj));
              });
        });
    };
};