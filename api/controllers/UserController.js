var UserModel = require('../models/users');
var UserType = require('../models/user_types');
var UserLogModel = require('../models/user_log');
var ContactModel = require('../models/contacts');
var NodeRSA = require('node-rsa');
var request = require('request');
var jwt = require('jsonwebtoken');
var nodemailer = require("nodemailer");
var mailerhbs = require('nodemailer-express-handlebars');
const { check, validationResult } = require('express-validator');
// var EmailController = require('./EmailTemplateController');

// var template_instance = new EmailController();
module.exports = function UserController() {

    /** 
     * <h1>[API] User Normal Signup</h1>
     * <p>This api use for user signup[registration] by email id</p>
     * @see API_BASE_URL+user/signup</a>
     * @param {String} email [*Mandatory][Valid Email]
     * @param {String} password [*Mandatory]
     * @param {String} Fullname [*Mandatory]
     * @param {String} mobile [Optional]
     * @header Content-Type: application/x-www-form-urlencoded
     * <pre>
     * {@code
     *    {
     *      "message": "Congratulation!! Your registration has been successfully made, Please verify your account",
     *      "status": "success",
     *      "result": ""
     *    }
     * </pre>
     * @author 
     */


    this.signup = function (req, res, next) {
        var currentTime = new Date().getTime();
        console.log('subrata lochan');
        UserModel.findOne({ email: req.body.email }, function (err, user) {
            var isNewUser = true;
            if (err) {
                return res.json(Utility.output(err, 'ERROR'));
            }
            if (user) {
                if (user.status == 1 || user.status === 0 || user.status == 2)
                 //   return res.send('email already used');
                 return res.json(Utility.output(Toster.EMAIL_ALREADY_USED, 'ERROR'));
            }
            if (isNewUser) {
                UserType.findOne({ slug: 'end-user' }, function (err, userType) {
                    var newUser = new UserModel();
                    newUser.fullName = req.body.fullName;
                    newUser.email = req.body.email.toLowerCase().toLowerCase();
                    newUser.status = 1;
                    newUser.password = newUser.encryptPassword(req.body.password);
                    newUser.v_code = newUser.generateVcode(currentTime);
                    newUser.mobile = Utility.escape(req.body.mobile);
                    newUser._user_type = userType;
                    newUser.date_of_creation = currentTime;
                    newUser.date_of_modification = currentTime;
                   // var uri = "http://103.14.97.154:3001?verification-code="+newUser.v_code;
                    newUser.save(function (err, result) {
                        if (err){
                                 console.log('error in saving');
                                 res.status(226).send({ status: 0, msg:'User already exist' });
                                }else{
                                    console.log('saved successfully');
                                    res.status(200).send({ status: 1, msg: 'User created successfully.' })
                                 /********If normal User send welcome and verification email*****/
                                    // let smtpTransport = nodemailer.createTransport({
                                    //     service: "gmail",
                                    //     host: "smtp.gmail.com",
                                    //     port: 587,
                                    //     secure: false,
                                    //     requireTLS: true,
                                    //     auth: {
                                    //         user: "subrata.bizzman@gmail.com",
                                    //         pass: "@bizz2019"
                                    //     }
                                    // });

                                    // const smtpTransport = nodemailer.createTransport({
                                    //     host: 'smtp.ethereal.email',
                                    //     port: 587,
                                    //     auth: {
                                    //         user: 'stefan0@ethereal.email',
                                    //         pass: 'ckekxSeAW1gmKutx7w'
                                    //     }
                                    // });


                                    // let mailOptions = {
                                    //     to: req.body.email.toLowerCase(),
                                    //     subject: req.body.subject,
                                    //     text: req.body.text,
                                    //     html: `
                                    //         <html xmlns="http://www.w3.org/1999/xhtml">

                                    //         <head>

                                    //           <!-- If you delete this meta tag, Half Life 3 will never be released. -->
                                    //           <meta name="viewport" content="width=device-width">
                                    //           <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                    //           <title>Cv-maker Emailer</title>
                                    //         </head>

                                    //         <body>
                                    //           <table class="wrapper" bgcolor="#ECEEF1" style="background-color:#ECEEF1;width:100%;">
                                    //             <tr>
                                    //               <td>
                                    //                 <!-- HEADER -->
                                    //                 <table class="head-wrap" style="margin: 0 auto;">
                                    //                   <tr>
                                    //                     <td></td>
                                    //                     <td class="header container logo" style="font-family:Source Sans Pro, Helvetica, sans-serif;color:#6f6f6f;display:block;margin:0 auto;max-width:600px;padding:20px">
                                    //                       <div class="content logo" style="display:block;margin:0 auto;max-width:650px;-webkit-font-smoothing:antialiased;padding:20px">
                                    //                         <!--<table>
                                    //                           <tr>
                                    //                             <td>

                                    //                             </td>
                                    //                           </tr>
                                    //                         </table>-->
                                    //                       </div>
                                    //                     </td>
                                    //                     <td></td>
                                    //                   </tr>
                                    //                 </table>
                                    //                 <!-- /HEADER -->
                                    //                 <!-- BODY -->
                                    //                 <table class="body-wrap" style="margin:0 auto;margin-bottom: 50px;">
                                    //                   <tr>
                                    //                     <td></td>
                                    //                     <td class="container transaction-mailer" bgcolor="#FFFFFF" style="font-family:Source Sans Pro, Helvetica, sans-serif;color:#6f6f6f;display:block;max-width:600px;margin:0 auto;padding:40px;border:1px solid #CED3D8;">
                                    //                       <div class="content" style="display:block;margin:0 auto;max-width:650px;padding:20px;-webkit-font-smoothing:antialiased">
                                    //                         <div class="receipt">
                                    //                           <div class="divider">
                                    //                             <div class="message">
                                    //         <img alt="our wonderful wistia logo" border="0" class="wistia-logo" height="46" src="images/bizzman-logo.png" width="100" style="display:block">
                                    //                                 <br>
                                    //                               <h1 class="emphasis" style="margin:0;padding:0;margin-bottom:20px;font-weight:700;margin-top:10px;-webkit-font-smoothing:antialiased;font-size:28px;line-height:130%;text-align:left;color:#54bbff">Thank you for subscribing to CVMaker
                                    //         .</h1>

                                    //                               <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">To complete your profile and start creating your distinctive CV, please click the following button.

                                    //                               <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">Want to know more? Reach us at <a href="#" target="_blank" style="color:#54bbff">address link</a></p>
                                    //                               <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">
                                    //                                 <br>
                                    //                                  <a href=`+uri+` style="color:#54bbff; background-color: #f80b82; padding: 10px 25px; color: #fff; text-decoration: none; border-radius: 4px;">Confirm My Email Address</a>
                                    //                               </p>
                                    //                             </div>
                                    //                           </div>

                                    //                           <div class="foot">
                                    //                             <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">
                                    //                               <strong>Thanks.</strong> 
                                    //                               <h3 style="text-align: center; ">FOLLOW US ON </h3>
                                    //                                                  <div style="text-align: center;">
                                    //                                                      <a href="https://www.facebook.com/Bizzmankolkata/?ref=br_rs">    <img src="images/unnamed.png" class="img-responsive" style="margin-right: 20px;" /></a>
                                    //                                                      <a href="https://twitter.com/BizzmanW"> <img src="images/twitter.png" class="img-responsive" style="margin-right: 20px;" /> </a>
                                    //                                                      <a href="https://www.linkedin.com/company/bizzman-design-marketing/">
                                    //                                                      <img src="images/insta.png">
                                    //                                                      </a>
                                    //                                                 </div>
                                    //                                                 <p style="text-align: center; margin-top: -10px;">Partner With BizzmanWeb<span style="color: #f76c6c; font-size: 38px;">♥</span></p>
                                    //                                                 <p style="text-align: center;">EN-62, Sector V, Salt Lake City, Kolkata, West Bengal 700091</p>
                                    //                                                 <p style="text-align: center;">Get in touch at <span style="color: #00b0bb">info@bizzmanweb.com</span> or<span style="color: #00b0bb;"> +918637360492 /+9184200442189 </span>.
                                    //         </p>
                                    //                               </p>
                                    //                           </div>
                                    //                         </div>
                                    //                       </div>
                                    //                       <!-- /content -->
                                    //                     </td>
                                    //                     <td></td>
                                    //                   </tr>
                                    //                   <tr>
                                    //                     <td></td>
                                    //                     <td></td>
                                    //                   </tr>
                                    //                 </table>
                                    //                 <!-- /BODY -->
                                    //               </td>
                                    //             </tr>
                                    //           </table>
                                    //           <!-- /wrapper -->

                                    //         </body>

                                    //         </html>` 

                                    // }

                                    // smtpTransport.sendMail(mailOptions, function (error, response) {
                                    //     if (error) {
                                    //         console.log(error);
                                    //         res.end("error");
                                    //     } else {
                                    //         // console.log(response.response);
                                    //         // res.end("sent");
                                    //         res.status(200).send(result);
                                    //     }
                                    // });
                                    /////////////////////temporary mail is off/////////////////
                                    //    var data = {
                                    //         company_name: SITE_SETTINGS.COMPANY_NAME,
                                    //         site_name: SITE_SETTINGS.SITE_NAME,
                                    //         contact_person_name: req.body.name,
                                    //         contact_person_email: req.body.email,
                                    //         contact_person_password: req.body.password,
                                    //         join_YourSite: constants.base_url + '#/login',
                                    //         signup_verify: constants.base_url + '#/verification/' + encodeURI(result.v_code),
                                    //         subscribe_YourSite: constants.base_url
                                    //     };
                                    // template_instance.load_template('WELCOME', data, function (error, html) {
                                    //     if (error)
                                    //     {
                                    //         return res.json(Utility.output(error, 'ERROR'));
                                    //     }
                                    // var opta = {
                                    //     to: req.body.email.toLowerCase(),
                                    //     sub: SITE_SETTINGS.SITE_NAME + ": "+Toster.THANK_CONTACT,
                                    //     body: html
                                    // }
                                    // Utility.sendMail(opta, function (err, info) {
                                    //     if (err) {
                                    //         return console.log(err);
                                    //     }
                                    //     console.log('Message sent: ' + info.response);
                                    // });
                                    // return res.status(200).json('successfully inserted', 'SUCCESS',result);
                                    //  res.status(200).json('successfully inserted', 'SUCCESS',result)
                                    // res.status(200).send(result);
                                    //  });
                            }
                        });
                });
            }
        });
    };


    /**
         * <h1>[API] User Facebook Signup</h1>
         * <p>This api use for facebook user signup[registration]</p>
         * @see API_BASE_URL+user/facebook_signup</a>
         * @param {String} email [*Mandatory][Valid Email]
         * @param {String} facebook_id [*Mandatory]
         * @param {String} name [*Mandatory]
         * @param {String} mobile [Optional]
         * @param {String} access_token [*Mandatory] [You will be got from FB return response]
         * @header Content-Type: application/x-www-form-urlencoded
         * <pre>
         * {@code
         *    {
         *      "message": "Congratulation!! Your registration has been successfully made, Please verify your account",
         *      "status": "success",
         *      "result": ""
         *    }
         * </pre>
         * @author 
         */
    this.facebook_signup = function (req, res, next) {
        // req.assert('access_token', Toster.FB_TOKEN_REQUIRED).notEmpty();
        // req.assert('email', Toster.EMAIL_REQUIRED).notEmpty();
        // req.assert('email', Toster.VALID_EMAIL).isEmail();
        // req.assert('firstName', Toster.NAME_REQUIRED).notEmpty();
        // req.assert('firstName', Toster.NAME_MINIMUM).optional().isLength({min: 3, max: 255});
        // var errors = req.validationErrors();
        // if (errors) {
        //     var messages = [];
        //     errors.forEach(function (error) {
        //         messages.push(error.msg);
        //     });
        //     return res.json(Utility.output(messages, 'ERROR'));
        // }
        var headers = {
            'Content-Type': 'application/json'
        }

        var options = {
            'url': 'http://localhost:3001/auth/facebook/callback',
            'method': 'POST',
            'headers': headers,
            'form': {
                'access_token': req.body.access_token,
            }
        };

        //Verify access_token is valid or not from facebook [OAuth2 Verification]
        request(options, function (error, response, body) {
            //  console.log(response);
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body);
                if (body.status == "error")
                    // return res.json(Utility.output(body.message, 'ERROR'));
                    res.status(201).send(body.message);
                returnObj = body.result;
                if (returnObj.user) {
                    if (returnObj.user.status == 1 || returnObj.user.status === 0 || returnObj.user.status == 2)
                        // return res.json(Utility.output(Toster.EMAIL_ALREADY_USED, 'ERROR'));
                        res.status(201).send(body.message);
                }
                //return res.send(returnObj);
                UserType.findOne({ slug: 'end-user' }, function (err, userType) {
                    var currentTime = new Date().getTime();
                    var newUser = new UserModel();
                    // newUser.email = Utility.escape(req.body.email.toLowerCase().toLowerCase());
                    newUser.email = req.body.email.toLowerCase().toLowerCase();
                    newUser.image = "";
                    newUser.password = null;
                    newUser.v_code = null;
                    newUser.facebook_id = returnObj.profile.id; // Assign Facebook ID
                    newUser.status = 1; //Default Activate Account no need to set any email verification code.
                    // newUser.image = Utility.escape(req.body.image); //Adding Facebook of Google Profile image path
                    newUser.fullname = (req.body.fullname) ? Utility.escape(req.body.fullname) : 'Utitled';
                    newUser._user_type = userType;
                    newUser.date_of_creation = currentTime;
                    newUser.date_of_modification = currentTime;
                    // if (req.body.image)
                    //     newUser.image = Utility.escape(req.body.image);
                    newUser.save(function (err, result) {
                        if (err) {
                            return res.json(Utility.output(err, 'ERROR'));
                        }
                        /********If facebook/google User send welcome email*****/
                        // var opta = {
                        //     'to': req.body.email.toLowerCase(),
                        //     'sub': "Your " + SITE_SETTINGS.SITE_NAME + Toster.ACCOUNT_CREATED,
                        //     'body': 'Hello ' + Utility.escape(req.body.name) + '<br/><br/>\n\
                        //             Welcome to ' + SITE_SETTINGS.SITE_NAME + '<br/>'
                        // };
                        // Utility.sendMail(opta, function (err, info) {
                        //     if (err) {
                        //         return console.log(err);
                        //     }
                        // });
                        // return res.json(Utility.output(Toster.REGISTRATION_COMPLETED, 'SUCCESS'));
                        res.status(200).send('SUCCESSFULL');
                    });
                });
            } else
                // return res.json(Utility.output(Toster.SOMETHING_WRONG, 'ERROR'));
                res.status(200).send('SOMETHING WENT WRONG');
        });
    };


    /**
       * <h1>[API] User Google Signup</h1>
       * <p>This api use for google user signup[registration]</p>
       * @see API_BASE_URL+user/google_signup</a>
       * @param {String} email [*Mandatory][Valid Email]
       * @param {String} google_id [*Mandatory]
       * @param {String} name [*Mandatory]
       * @param {String} mobile [Optional]
       * @param {String} access_token [*Mandatory] [You will be got from FB return response]
       * @header Content-Type: application/x-www-form-urlencoded
       * <pre>
       * {@code
       *    {
       *      "message": "Congratulation!! Your registration has been successfully made, Please verify your account",
       *      "status": "success",
       *      "result": ""
       *    }
       * </pre>
       * @author 
       */
    this.google_signup = function (req, res, next) {
        req.assert('access_token', Toster.GOOGLE_TOKEN_REQUIRED).notEmpty();
        req.assert('email', Toster.EMAIL_REQUIRED).notEmpty();
        req.assert('email', Toster.VALID_EMAIL).isEmail();
        req.assert('name', Toster.NAME_REQUIRED).notEmpty();
        req.assert('name', Toster.NAME_MINIMUM).optional().isLength({ min: 3, max: 255 });

        var errors = req.validationErrors();
        if (errors) {
            var messages = [];
            errors.forEach(function (error) {
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages, 'ERROR'));
        }
        //Verify access_token is valid or not from google [OAuth2 Verification]
        var headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        };
        var options = {
            'url': constants.auth_server + '/verify_google_auth',
            'method': 'POST',
            'headers': headers,
            'form': {
                'access_token': req.body.access_token,
            }
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body);
                if (body.status == "error")
                    return res.json(Utility.output(body.message, 'ERROR'));
                returnObj = body.result;
                if (returnObj.user) {
                    if (returnObj.user.status == 1 || returnObj.user.status === 0 || returnObj.user.status == 2)
                        return res.json(Utility.output(Toster.EMAIL_ALREADY_USE, 'ERROR'));
                }
                //return res.send(returnObj);
                UserType.findOne({ slug: 'end-user' }, function (err, userType) {
                    var currentTime = new Date().getTime();
                    var newUser = new UserModel();
                    newUser.email = Utility.escape(req.body.email.toLowerCase().toLowerCase());
                    newUser.image = "";
                    newUser.password = null;
                    newUser.v_code = null;
                    newUser.google_id = returnObj.profile.id; // Assign Google ID
                    newUser.status = 1; //Default Activate Account no need to set any email verification code.
                    newUser.image = Utility.escape(req.body.image); //Adding Facebook of Google Profile image path
                    newUser.name = (req.body.name) ? Utility.escape(req.body.name) : 'Utitled';
                    newUser._user_type = userType;
                    newUser.date_of_creation = currentTime;
                    newUser.date_of_modification = currentTime;
                    if (req.body.image)
                        newUser.image = Utility.escape(req.body.image);
                    newUser.save(function (err, result) {
                        if (err) {
                            return res.json(Utility.output(err, 'ERROR'));
                        }
                        /********If facebook/google User send welcome email*****/
                        var opta = {
                            'to': req.body.email.toLowerCase(),
                            'sub': "Your " + SITE_SETTINGS.SITE_NAME + Toster.ACCOUNT_CREATED,
                            'body': 'Hello ' + Utility.escape(req.body.name) + '<br/><br/>\n\
                                    Welcome to ' + SITE_SETTINGS.SITE_NAME + '<br/>'
                        };
                        Utility.sendMail(opta, function (err, info) {
                            if (err) {
                                return console.log(err);
                            }
                        });
                        return res.json(Utility.output(Toster.REGISTRATION_COMPLETED, 'SUCCESS'));
                    });
                });
            } else
                return res.json(Utility.output(Toster.SOMETHING_WRONG, 'ERROR'));
        });
    };


    /**
     * <h1>[API] User Normal Login</h1>
     * <p>This api use for user login by email id</p>
     * @see API_BASE_URL+user/login</a>
     * @param {String} email [*Mandatory][Valid Email]
     * @param {String} password [*Mandatory]
     * @param {String} browser_token [*Mandatory]
     * @header Content-Type: application/x-www-form-urlencoded
     * <pre>
     * {@code
     *    {
     *      "message": "Successfully logged in",
     *      "status": "success",
     *      "result": {
     *        "user": {
     *          "_id": "582ec52757af7013c0bf2f6c",
     *          "date_of_modification": 1479460135101,
     *          "date_of_creation": 1479460135101,
     *          "mobile": "",
     *          "name": "",
     *          "email": "",
     *          "image": "",
     *          "status": 1,
     *          "custom_status": -99
     *        },
     *        "auth_token": ""
     *      }
     *    }
     * </pre>
     * @author 
     */
    this.login = function (req, res, next) {
        //  var requestParser = require('ua-parser').parse(req.headers['user-agent']);
        var currentTime = new Date().getTime();
        //  req.assert('email', Toster.EMAIL_REQUIRED).notEmpty();
        //  req.assert('email', Toster.VALID_EMAIL).isEmail();
        //  req.assert('password', Toster.PASSWORD_REQUIRED).notEmpty(); //Adding Password Validation rule
        //  req.assert('browser_token','Browser Token is required').notEmpty();
        //   console.log('USER:',req.body.email);
        // var errors = req.validationErrors();
        // if (errors) {
        //     var messages = [];
        //     errors.forEach(function (error) {
        //         messages.push(error.msg);
        //     });
        //     return res.json(Utility.output(messages, 'ERROR'));
        // }
        var searchKey = {
            'email': req.body.email.toLowerCase()
        };
       
        UserModel.findOne(searchKey).populate('_user_type').exec(function (err, user) {
            if (err) {
                return res.json(Utility.output(err, 'ERROR'));
            }
            if (!user) {
               // res.status(401).send({ status: 0, msg: 'Unauthorized.' });
                return res.json(Utility.output(Toster.USER_NOT_FOUND, 'ERROR'));
            }
            if (user) {
                console.log('lochan',user);
                // var token = jwt.sign({ id: user._id, slug: user._user_type.slug }, "586", {
                var token = jwt.sign({ id: user._id }, "586", {
                    expiresIn: 86400 // expires in 24 hours
                });
                var tokenUserDetails = {
                    "user": {
                        "_id": user._id,
                        "date_of_modification": user.date_of_modification,
                        "date_of_creation": user.date_of_creation,
                        "fullName": user.fullName,
                        "userId": user.userId,
                        "role": user.role,
                        "email": user.email,
                        // "mobile": user.mobile,
                        // "image": user.image,
                        // "status": 1,
                        // "custom_status": -99,
                        // "last_activity": user.last_activity,
                        // "user_type": user._user_type.type
                    },
                    "auth_token": token
                };
            if (!user.validPassword(req.body.password)) {
                  //  res.status(401).send({ status: 0, msg: 'Unauthorized.' });
                    return res.json(Utility.output(Toster.INVALID_PASSWORD, 'ERROR'));
                }
                if (user.status == 0)
                    return res.json(Utility.output(Toster.ACCOUNT_VERIFICATION_REQUIRED, 'ERROR'));
                if (user.status == 3)
                    return res.json(Utility.output(Toster.ACCOUNT_REMOVE, 'ERROR'));
                if (user.status == 2)
                    return res.json(Utility.output(Toster.ACCOUNT_BLOCKED_BY_ADMIN, 'ERROR'));
                if (user.status == 1)
                    return res.json(Utility.output(Toster.LOGIN_SUCCESS, 'SUCCESS', tokenUserDetails));
                if (user.status == 4)
                    return res.json(Utility.output(Toster.LOGIN_SUCCESS, 'SUCCESS', tokenUserDetails));
                if (user.status == 5)
                    return res.json(Utility.output(Toster.LOGIN_SUCCESS, 'SUCCESS', tokenUserDetails));
                if (user.status == 6)
                    return res.json(Utility.output(Toster.LOGIN_SUCCESS, 'SUCCESS', tokenUserDetails));

                // if (user.image)
                //     if (user.image.indexOf('http://') > -1 || user.image.indexOf('https://') > -1) {
                //     } else {
                //         tokenUserDetails.image = constants.profile_image_public_link + user.image;
                //     }
                //Create Authentication Token
                var headers = {
                    'Content-Type': 'application/json'
                };
                // var headers = {
                //     'Content-Type': 'application/x-www-form-urlencoded'
                // };
            }
        });
    };


    /**
       * <h1>[API] User Facebook Login</h1>
       * <p>This api use for user login by facebook account</p>
       * @see API_BASE_URL+user/facebook_login</a>
       * @param {String} access_token [*Mandatory][You will get this access token from FB API]
       * @param {String} facebook_id [*Mandatory]
       * @param {String} browser_token [*Mandatory]
       * @header Content-Type: application/x-www-form-urlencoded
       * <pre>
       * {@code
       *    {
       *      "message": "Successfully logged in",
       *      "status": "success",
       *      "result": {
       *        "user": {
       *          "_id": "582ec52757af7013c0bf2f6c",
       *          "date_of_modification": 1479460135101,
       *          "date_of_creation": 1479460135101,
       *          "mobile": "",
       *          "name": "Subrata Nandi",
       *          "email": "subrata.bizman@gmail.com",
       *          "image": "",
       *          "status": 1,
       *          "custom_status": -99
       *        },
       *        "auth_token": "JWT eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ODJlYzUyNzU3YWY3MDEzYzBiZjJmNmMiLCJkYXRlX29mX21vZGlmaWNhdGlvbiI6MTQ3OTQ2MDEzNTEwMSwiZGF0ZV9vZl9jcmVhdGlvbiI6MTQ3OTQ2MDEzNTEwMSwibW9iaWxlIjoiIiwibmFtZSI6IlNvaGFtIEtyaXNobmEgUGF1bCIsImVtYWlsIjoic3VwcG9ydC5mZHh6QGdtYWlsLmNvbSIsImltYWdlIjoiIiwic3RhdHVzIjoxLCJjdXN0b21fc3RhdHVzIjotOTksImlhdCI6MTQ3OTQ2MDU3NCwiYXVkIjoiWXV2aXRpbWUuY29tLmF1IiwiaXNzIjoiWXV2aXRpbWUuY29tIn0.dWLcuykEvGALyBUt3uAytnuD6QfNtNaOvgf4pN29sLHqseVZz8uKhltPR4PPazVNJkRF1oXTvapjqagKKP4k5Q"
       *      }
       *    }
       * </pre>
       * @author Subrata nandi
       */
    this.facebook_login = function (req, res, next) {
        var requestParser = require('ua-parser').parse(req.headers['user-agent']);
        var currentTime = new Date().getTime();
        req.assert('access_token', Toster.FB_TOKEN_REQUIRED).notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            var messages = [];
            errors.forEach(function (error) {
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages, 'ERROR'));
        }

        //Verify access_token is valid or not from facebook [OAuth2 Verification]
        var headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        var options = {
            'url': constants.auth_server + '/verify_facebook_auth',
            'method': 'POST',
            'headers': headers,
            'form': {
                'access_token': req.body.access_token,
            }
        };
        //Verify Facebook Auth Token
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body);
                if (body.status == "error")
                    return res.json(Utility.output(body.message, 'ERROR'));
                returnObj = body.result;
                if (returnObj.user) {
                    if (returnObj.user.status == 3)
                        return res.json(Utility.output(Toster.ACCOUNT_REMOVE, 'ERROR'));
                    if (returnObj.user.status == 2)
                        return res.json(Utility.output(Toster.ACCOUNT_BLOCKED_BY_ADMIN, 'ERROR'));
                } else {
                    return res.json(Utility.output(Toster.USER_DOES_NOT_EXIST, 'ERROR'));
                }

                //Token Successfully Verified
                var tokenUserDetails = {
                    "_id": returnObj.user._id,
                    "date_of_modification": returnObj.user.date_of_modification,
                    "date_of_creation": returnObj.user.date_of_creation,
                    "mobile": returnObj.user.mobile,
                    "name": returnObj.user.name,
                    "email": returnObj.user.email,
                    "image": returnObj.user.image,
                    "facebook_id": returnObj.profile.id,
                    "google_id": null,
                    "linkedin_id": null,
                    "status": 1,
                    "custom_status": -99,
                    "last_activity": returnObj.user.last_activity
                };
                if (returnObj.user.image)
                    if (returnObj.user.image.indexOf('http://') > -1 || returnObj.user.image.indexOf('https://') > -1) {
                    } else {
                        tokenUserDetails.image = constants.profile_image_public_link + returnObj.user.image;
                    }
                //Create Authentication Token

                headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }

                options = {
                    'url': constants.auth_server + '/login',
                    'method': 'POST',
                    'headers': headers,
                    'form': {
                        'user_id': returnObj.user._id.toString(),
                        'device': requestParser.device.family,
                        'browser': requestParser.ua.toString(),
                        'os': requestParser.os.toString(),
                        'ip': (req.ip === '::1' || req.ip === '::ffff:127.0.0.1') ? 'localhost' : req.ip,
                    }
                };
                //Curl Call to login this user in Auth Server and Get Auth_Token
                request(options, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        body = JSON.parse(body);
                        if (body.status == "error")
                            return res.json(Utility.output(body.message, 'ERROR'));
                        UserModel.update(
                            { _id: tokenUserDetails._id },
                            { $set: { last_activity: currentTime } }, function (err, numberUpdate) {
                                if (err)
                                    return res.json(Utility.output(Toster.SOMETHING_WRONG, 'ERROR'));
                            });

                        var output = {
                            'user': tokenUserDetails,
                            'auth_token': body.result
                        };
                        return res.json(Utility.output(Toster.LOGIN_SUCCESS, 'SUCCESS', output));
                    } else
                        return res.json(Utility.output(Toster.LOGIN_ERROR, 'ERROR'));
                });
            } else
                return res.json(Utility.output(Toster.FB_TOEN_ERROR, 'ERROR'));
        });
    };


    /**
    * <h1>[API] User Google Login</h1>
    * <p>This api use for user login by google account</p>
    * @see API_BASE_URL+user/google_login</a>
    * @param {String} access_token [*Mandatory][You will get this access token from FB API]
    * @param {String} google_id [*Mandatory]
    * @param {String} browser_token [*Mandatory]
    * @header Content-Type: application/x-www-form-urlencoded
    * <pre>
    * {@code
    *    {
    *      "message": "Successfully logged in",
    *      "status": "success",
    *      "result": {
    *        "user": {
    *          "_id": "582ec52757af7013c0bf2f6c",
    *          "date_of_modification": 1479460135101,
    *          "date_of_creation": 1479460135101,
    *          "mobile": "",
    *          "name": "Subrata Nandi",
    *          "email": "support.fdxz@gmail.com",
    *          "image": "",
    *          "status": 1,
    *          "custom_status": -99
    *        },
    *        "auth_token": "JWT eyJhbGciOiJSUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWQiOiI1ODJlYzUyNzU3YWY3MDEzYzBiZjJmNmMiLCJkYXRlX29mX21vZGlmaWNhdGlvbiI6MTQ3OTQ2MDEzNTEwMSwiZGF0ZV9vZl9jcmVhdGlvbiI6MTQ3OTQ2MDEzNTEwMSwibW9iaWxlIjoiIiwibmFtZSI6IlNvaGFtIEtyaXNobmEgUGF1bCIsImVtYWlsIjoic3VwcG9ydC5mZHh6QGdtYWlsLmNvbSIsImltYWdlIjoiIiwic3RhdHVzIjoxLCJjdXN0b21fc3RhdHVzIjotOTksImlhdCI6MTQ3OTQ2MDU3NCwiYXVkIjoiWXV2aXRpbWUuY29tLmF1IiwiaXNzIjoiWXV2aXRpbWUuY29tIn0.dWLcuykEvGALyBUt3uAytnuD6QfNtNaOvgf4pN29sLHqseVZz8uKhltPR4PPazVNJkRF1oXTvapjqagKKP4k5Q"
    *      }
    *    }
    * </pre>
    * @author Subrata Nandi
    */
    this.google_login = function (req, res, next) {
        var requestParser = require('ua-parser').parse(req.headers['user-agent']);
        var currentTime = new Date().getTime();
        req.assert('access_token', Toster.GOOGLE_TOKEN_REQUIRED).notEmpty();

        var errors = req.validationErrors();
        if (errors) {
            var messages = [];
            errors.forEach(function (error) {
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages, 'ERROR'));
        }
        //Verify access_token is valid or not from google [OAuth2 Verification]
        var headers = {
            'Content-Type': 'application/x-www-form-urlencoded'
        }

        var options = {
            'url': constants.auth_server + '/verify_google_auth',
            'method': 'POST',
            'headers': headers,
            'form': {
                'access_token': req.body.access_token,
            }
        };
        //Verify Facebook Auth Token
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body);
                if (body.status == "error")
                    return res.json(Utility.output(body.message, 'ERROR'));
                returnObj = body.result;
                if (returnObj.user) {
                    if (returnObj.user.status == 3)
                        return res.json(Utility.output(Toster.ACCOUNT_REMOVE, 'ERROR'));
                    if (returnObj.user.status == 2)
                        return res.json(Utility.output(Toster.ACCOUNT_BLOCKED_BY_ADMIN, 'ERROR'));
                } else {
                    return res.json(Utility.output(Toster.USER_DOES_NOT_EXIST, 'ERROR'));
                }

                //Token Successfully Verified
                var tokenUserDetails = {
                    "_id": returnObj.user._id,
                    "date_of_modification": returnObj.user.date_of_modification,
                    "date_of_creation": returnObj.user.date_of_creation,
                    "mobile": returnObj.user.mobile,
                    "name": returnObj.user.name,
                    "email": returnObj.user.email,
                    "image": returnObj.user.image,
                    "facebook_id": null,
                    "google_id": returnObj.profile.id,
                    "linkedin_id": null,
                    "status": 1,
                    "custom_status": -99,
                    "last_activity": returnObj.user.last_activity
                };
                if (returnObj.user.image)
                    if (returnObj.user.image.indexOf('http://') > -1 || returnObj.user.image.indexOf('https://') > -1) {
                    } else {
                        tokenUserDetails.image = constants.profile_image_public_link + returnObj.user.image;
                    }
                //Create Authentication Token

                headers = {
                    'Content-Type': 'application/x-www-form-urlencoded'
                }

                options = {
                    'url': constants.auth_server + '/login',
                    'method': 'POST',
                    'headers': headers,
                    'form': {
                        'user_id': returnObj.user._id.toString(),
                        'device': requestParser.device.family,
                        'browser': requestParser.ua.toString(),
                        'os': requestParser.os.toString(),
                        'ip': (req.ip === '::1' || req.ip === '::ffff:127.0.0.1') ? 'localhost' : req.ip,
                    }
                };
                //Curl Call to login this user in Auth Server and Get Auth_Token
                request(options, function (error, response, body) {
                    if (!error && response.statusCode == 200) {
                        body = JSON.parse(body);
                        if (body.status == "error")
                            return res.json(Utility.output(body.message, 'ERROR'));
                        UserModel.update(
                            { _id: tokenUserDetails._id },
                            { $set: { last_activity: currentTime } }, function (err, numberUpdate) {
                                if (err)
                                    return res.json(Utility.output(Toster.SOMETHING_WRONG, 'ERROR'));
                            });

                        var output = {
                            'user': tokenUserDetails,
                            'auth_token': body.result
                        };
                        return res.json(Utility.output(Toster.LOGIN_SUCCESS, 'SUCCESS', output));
                    } else
                        return res.json(Utility.output(Toster.LOGIN_ERROR, 'ERROR'));
                });
            } else
                return res.json(Utility.output(Toster.FB_TOEN_ERROR, 'ERROR'));
        });
    };



    this.logout = function (req, res, next) {
        var headers = {
            'Authorization': req.headers.authorization,
            'Content-Type': 'application/x-www-form-urlencoded'
        }
        var options = {
            // 'url': constants.auth_server + '/logout',
            'url': 'localhost:3001/users/logout',
            'method': 'POST',
            'headers': headers,
        };
        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                return res.send(body);
            } else
                return res.json(Utility.output(Toster.SOMETHING_WRONG, 'ERROR'));
        });
    };

    /**
    * <h1>[API] Send a forget password request [Forget Password]</h1>
    * <p>This api use for user can request for a new password. 
    * Generally a Reset password URL will be sent to that user email id
    * That Link/URL will have an expiry time</p>
    * @see API_BASE_URL+user/request_forget_password</a>
    * @param {String} email [*Mandatory][Valid Email]
    * @header Content-Type: application/json //x-www-form-urlencoded
    * <pre>
    * {@code
    *    {
    *    "message": "Reset password url has been sent",
    *    "status": "success",
    *    "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata nandi
    */
    this.request_forget_password = function (req, res, next) {
        var currentTime = new Date().getTime();
        // req.assert('email', 'Email is required').notEmpty();
        // req.assert('email', 'Enter valid email').isEmail();
        // var errors = req.validationErrors();
        // if (errors) {
        //     var messages = [];
        //     errors.forEach(function (error) {
        //         messages.push(error.msg);
        //     });
        //     return res.json(Utility.output(messages, 'ERROR'));
        // }
      // console.log('tunu',req.body);
        UserModel.findOne({ email: req.body.email.toLowerCase() }, function (err, user) {
            console.log('lochan',user);
            if (err)
                return res.json(Utility.output(Toster.FETCH_INFO_ERROR, 'ERROR'));
            if (!user)
                return res.json(Utility.output(Toster.USER_INFO_NOT_FOUND, 'ERROR'));
            if (user.status == 0)
                return res.json(Utility.output(Toster.ACCOUNT_VERIFICATION_REQUIRED, 'ERROR'));
            if (user.status == 3)
                return res.json(Utility.output(Toster.ACCOUNT_REMOVE, 'ERROR'));
            if (user.status == 2)
                return res.json(Utility.output(Toster.ACCOUNT_BLOCKED_BY_ADMIN, 'ERROR'));
            if (user.facebook_id)
                return res.json(Utility.output(Toster.ACCOUNT_WITH_FB, 'ERROR'));
            if (user.google_id)
                return res.json(Utility.output(Toster.ACCOUNT_WITH_GOOGLE, 'ERROR'));

            var passwordRequest = {
                'expire_on': (new Date().getTime() + (24 * 3600 * 1000)),
                'user_id': user._id
            };
            //Generate "forget_password_request" token.
            // var forget_password_request = new NodeRSA(constants.getPublicKey()).encrypt(JSON.stringify(passwordRequest), 'base64');
            var forget_password_request = '7777subrata';
            console.log('madhupur',user);
            UserModel.updateOne(
                { _id: user._id },
                { $set: { 'forget_password_request': forget_password_request } }, function (err, numberUpdate) {
                    if (err)
                        return res.json(Utility.output(err, 'ERROR'));
                    if (numberUpdate) {
                        let smtpTransport = nodemailer.createTransport({
                            service: "gmail",
                            host: "smtp.gmail.com",
                            port: 587,
                            secure: false,
                            requireTLS: true,
                            auth: {
                                user: "subrata.bizzman@gmail.com",
                                pass: "@bizz2019"
                            }
                        });


                        // let mailOptions = {
                        //     to: req.body.email.toLowerCase(),
                        //     subject: 'password request',// req.body.subject,
                        //     text: 'test for password'//,req.body.text
                        // }
//---------------------------------------------------------------------------
                                   console.log('mail option',user.email);
                                   console.log('body',req.body.email);
                                    let mailOptions = {
                                        to: user.email,
                                        subject: 'Forgot your password',
                                        text: 'Please keep in touch with us',
                                        html: `
                                            <html xmlns="http://www.w3.org/1999/xhtml">

                                            <head>

                                              <!-- If you delete this meta tag, Half Life 3 will never be released. -->
                                              <meta name="viewport" content="width=device-width">
                                              <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                                              <title>Cv-maker Emailer</title>
                                            </head>

                                            <body>
                                              <table class="wrapper" bgcolor="#ECEEF1" style="background-color:#ECEEF1;width:100%;">
                                                <tr>
                                                  <td>
                                                    <!-- HEADER -->
                                                    <table class="head-wrap" style="margin: 0 auto;">
                                                      <tr>
                                                        <td></td>
                                                        <td class="header container logo" style="font-family:Source Sans Pro, Helvetica, sans-serif;color:#6f6f6f;display:block;margin:0 auto;max-width:600px;padding:20px">
                                                          <div class="content logo" style="display:block;margin:0 auto;max-width:650px;-webkit-font-smoothing:antialiased;padding:20px">
                                                            <!--<table>
                                                              <tr>
                                                                <td>

                                                                </td>
                                                              </tr>
                                                            </table>-->
                                                          </div>
                                                        </td>
                                                        <td></td>
                                                      </tr>
                                                    </table>
                                                    <!-- /HEADER -->
                                                    <!-- BODY -->
                                                    <table class="body-wrap" style="margin:0 auto;margin-bottom: 50px;">
                                                      <tr>
                                                        <td></td>
                                                        <td class="container transaction-mailer" bgcolor="#FFFFFF" style="font-family:Source Sans Pro, Helvetica, sans-serif;color:#6f6f6f;display:block;max-width:600px;margin:0 auto;padding:40px;border:1px solid #CED3D8;">
                                                          <div class="content" style="display:block;margin:0 auto;max-width:650px;padding:20px;-webkit-font-smoothing:antialiased">
                                                            <div class="receipt">
                                                              <div class="divider">
                                                                <div class="message">
                                            <img alt="our wonderful wistia logo" border="0" class="wistia-logo" height="46" src="images/bizzman-logo.png" width="100" style="display:block">
                                                                    <br>
                                                                  <h1 class="emphasis" style="margin:0;padding:0;margin-bottom:20px;font-weight:700;margin-top:10px;-webkit-font-smoothing:antialiased;font-size:28px;line-height:130%;text-align:left;color:#54bbff">Thank you for subscribing to CVMaker
                                            .</h1>

                                                                  <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">To complete your profile and start creating your distinctive CV, please click the following button.

                                                                  <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">Want to know more? Reach us at <a href="#" target="_blank" style="color:#54bbff">NOthing</a></p>
                                                                  <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">
                                                                    <br>
                                                                      <a href="http://162.213.248.35:4204/reset-password" style="color:#54bbff; background-color: #f80b82; padding: 10px 25px; color: #fff; text-decoration: none; border-radius: 4px;">Reset My Password</a>
                                                                  </p>
                                                                </div>
                                                              </div>

                                                              <div class="foot">
                                                                <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">
                                                                  <strong>Thanks.</strong> 
                                                                  <h3 style="text-align: center; ">FOLLOW US ON </h3>
                                                                                     <div style="text-align: center;">
                                                                                         <a href="https://www.facebook.com/Bizzmankolkata/?ref=br_rs">    <img src="images/unnamed.png" class="img-responsive" style="margin-right: 20px;" /></a>
                                                                                         <a href="https://twitter.com/BizzmanW"> <img src="images/twitter.png" class="img-responsive" style="margin-right: 20px;" /> </a>
                                                                                         <a href="https://www.linkedin.com/company/bizzman-design-marketing/">
                                                                                         <img src="images/insta.png">
                                                                                         </a>
                                                                                    </div>
                                                                                    <p style="text-align: center; margin-top: -10px;">Partner With BizzmanWeb<span style="color: #f76c6c; font-size: 38px;">♥</span></p>
                                                                                    <p style="text-align: center;">EN-62, Sector V, Salt Lake City, Kolkata, West Bengal 700091</p>
                                                                                    <p style="text-align: center;">Get in touch at <span style="color: #00b0bb">info@bizzmanweb.com</span> or<span style="color: #00b0bb;"> +918637360492 /+9184200442189 </span>.
                                            </p>
                                                                  </p>
                                                              </div>
                                                            </div>
                                                          </div>
                                                          <!-- /content -->
                                                        </td>
                                                        <td></td>
                                                      </tr>
                                                      <tr>
                                                        <td></td>
                                                        <td></td>
                                                      </tr>
                                                    </table>
                                                    <!-- /BODY -->
                                                  </td>
                                                </tr>
                                              </table>
                                              <!-- /wrapper -->

                                            </body>

                                            </html>` 

                                    }
//---------------------------------------------------------------------------
                        smtpTransport.sendMail(mailOptions, function (error, response) {
                            if (error) {
                              //  console.log(error);
                                res.end("error");
                            } else {
                                console.log(response.response);
                                // res.end("sent");
                                res.status(200).send(response);
                            }
                        });
                        // var data = {
                        //     company_name: SITE_SETTINGS.COMPANY_NAME,
                        //     site_name: SITE_SETTINGS.SITE_NAME,
                        //     contact_person_name: user.name,
                        //     join_yuvitime: constants.base_url + '#/login',
                        //     subscribe_yuvitime: constants.base_url,
                        //     forget_pass: constants.base_url + '#/password_request/' + encodeURI(forget_password_request)
                        // };
                        // template_instance.load_template('FORGOT_PASSWORD', data, function (error, html) {
                        //     if (error) {
                        //         return res.json(Utility.output(error, 'ERROR'));
                        //     }
                        // var opta = {
                        //     to: user.email,
                        //     sub: 'password requesst',//SITE_SETTINGS.SITE_NAME + Toster.NEW_PASSWORD_REQUEST,
                        //     body: 'html'
                        // }
                        // Utility.sendMail(opta, function (err, info) {
                        //     if (err) {
                        //         return res.json(Utility.output(Toster.SEND_PASSWORD_ERROR, 'ERROR'));
                        //     } else {
                        //         return res.json(Utility.output(Toster.RESET_PASSWORD_EMAIL, 'SUCCESS'));
                        //       //  console.log('Message sent: ' + info.response);
                        //     }

                        // });
                        // });
                        //------------------------------------------------------
                        //                    var opta={
                        //                        'to':user.email,
                        //                        'sub':SITE_SETTINGS.SITE_NAME+" New Password Request",
                        //                        'body':'Hello '+user.name+'<br/><br/>\n\
                        //                                Welcome to '+SITE_SETTINGS.SITE_NAME+'<br/>\n\
                        //                                Your new password request link is below<br/>\n\
                        //                                <br/><a style="background-color: #ff930c;\n\
                        //                                border-radius: 0px;\n\
                        //                                color: #fff;\n\
                        //                                cursor: pointer;\n\
                        //                                display: inline-block;\n\
                        //                                font-size: 14px;\n\
                        //                                font-weight: 700;\n\
                        //                                line-height: 20px;\n\
                        //                                min-width: 180px;\n\
                        //                                padding: 14px 30px;\n\
                        //                                text-align: center;\n\
                        //                                text-transform: uppercase;\n\
                        //                                vertical-align: top;\n\
                        //                                text-decoration: none;" href="'+constants.base_url+'#/password_request/'+encodeURI(forget_password_request)+'">Click Here</a><br/>to set new password for your account.\n\
                        //                                <br/>or, Paste the below link on your browser.<br/>\n\
                        //                                <a href="'+constants.base_url+'#/password_request/'+encodeURI(forget_password_request)+'">'+constants.base_url+'#/password_request/'+encodeURI(forget_password_request)+'</a>\n\
                        //                                <br/>'
                        //                    };
                        //                    Utility.sendMail(opta,function(err,info){
                        //                        if(err){
                        //                            return res.json(Utility.output('Error in send password url','ERROR'));
                        //                        }
                        //                        else
                        //                        {
                        //                            return res.json(Utility.output('Reset password url has been sent to your email address','SUCCESS'));
                        //                        }
                        //                    });
                    }
                });
        });
    };


    /**
    * <h1>[API] Verify Forget Password Token</h1>
    * <p>This api use for verification the url what was generated for reset password.
    * Generally Server check the token that the token is valid or nor before proceed to Set new password</p>
    * @see API_BASE_URL+user/verify_forget_password_token</a>
    * @param {String} password_request_token [*Mandatory]
    * @header Content-Type: application/x-www-form-urlencoded
    * <pre>
    * {@code
    *{
    *    "message": "Valid request token",
    *    "status": "success",
    *    "result": ""
    *  }
    *}
    * </pre>
    * @author Subrata nandi
    */
    this.verify_forget_password_token = function (req, res, next) {
        // req.assert('password_request_token', Toster.PASSWORD_TOKEN).notEmpty();
        // var errors = req.validationErrors();
        // if (errors) {
        //     var messages = [];
        //     errors.forEach(function (error) {
        //         messages.push(error.msg);
        //     });
        //     return res.json(Utility.output(messages, 'ERROR'));
        // }
        // try {
        //     var decodedContain = new NodeRSA(constants.getPrivateKey()).decrypt(req.body.password_request_token, 'utf8');
        // } catch (e) {
        //     return res.json(Utility.output(Toster.INVALID_REQUEST_TOKEN, 'ERROR'));
        // }
        var extractToken = '444444444444444'; //JSON.parse('decodedContain');
        if (!extractToken)
            return res.json(Utility.output(Toster.INVALID_REQUEST_TOKEN, 'ERROR'));
        if (extractToken.expire_on < new Date().getTime())
            return res.json(Utility.output(Toster.REQUEST_EXPIRED, 'ERROR'));
               
       // extractToken.user_id = '5d9ec1fa43c93f27d6a95540';
        UserModel.findOne({ _id: '5d9ec1fa43c93f27d6a95540' }, function (err, user) {
            if (err)
                return res.json(Utility.output(err, 'ERROR'));
            if (!user)
                return res.json(Utility.output(Toster.USER_NOT_FOUND, 'ERROR'));
            // if (user.forget_password_request != req.body.password_request_token)
            //     return res.json(Utility.output(Toster.INVALID_REQUEST_TOKEN, 'ERROR'));
            return res.json(Utility.output(Toster.VALID_TOKEN, 'SUCCESS'));
        });
    };



    /**
    * <h1>[API] Email Verification and Account Activation</h1>
    * <p>This api use for verification of user email that the user is genuin user or not.
    * If success then account will be activated</p>
    * @see API_BASE_URL+user/verification</a>
    * @param {String} verification_code [*Mandatory]
    * @header Content-Type: application/x-www-form-urlencoded
    * <pre>
    * {@code
    *    {
    *      "message": "Your account verification has been completed",
    *      "status": "success",
    *      "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata Nandi
    */
    this.email_verification = function (req, res, next) {
        // req.assert('verification_code', 'Verification Code is required').notEmpty();
        // var errors = req.validationErrors();
        // if (errors) {
        //     var messages = [];
        //     errors.forEach(function (error) {
        //         messages.push(error.msg);
        //     });
        //     return res.json(Utility.output(messages, 'ERROR'));
        // }
      console.log('88899998',req.body.verification-code);
        UserModel.findOne({ 'v_code': decodeURI(req.body.verification-code) }, function (err, user) {
            if (err)
                return res.json(Utility.output(err, 'ERROR'));
            if (!user)
                return res.json(Utility.output(Toster.INVALID_VERIFICATION, 'ERROR'));

            UserModel.update(
                { _id: user._id },
                { $set: { 'v_code': null, 'status': 1 } }, function (err, numberUpdate) {
                    if (err)
                        return res.json(Utility.output(err, 'ERROR'));
                    if (numberUpdate)
                        return res.json(Utility.output(Toster.VERIFICATION_COMPLETED, 'SUCCESS'));
                });
        });
    };


    /**
    * <h1>[API] Resend verification url</h1>
    * <p>This api use for resend verification email to user email</p>
    * @see API_BASE_URL+user/resend_verification</a>
    * @param {String} email [*Mandatory][Valid Email]
    * @header Content-Type: application/x-www-form-urlencoded
    * <pre>
    * {@code
    *    {
    *      "message": "Verification mail sent to "example@example.com" email address",
    *      "status": "success",
    *      "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata nandi
    */
    this.resend_email_verification = function (req, res, next) {
        req.assert('email', 'Email is required').notEmpty();
        req.assert('email', 'Enter valid email').isEmail();
        var errors = req.validationErrors();
        if (errors) {
            var messages = [];
            errors.forEach(function (error) {
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages, 'ERROR'));
        }
        UserModel.findOne({ 'email': req.body.email.toLowerCase() }, function (err, user) {
            if (err)
                return res.json(Utility.output(err, 'ERROR'));
            if (!user)
                return res.json(Utility.output(Toster.USER_NOT_FOUND, 'ERROR'));

            if (user.status == 1)
                return res.json(Utility.output(Toster.ACCOUNT_ALREADY_ACTIVATED, 'ERROR'));
            if (user.status == 2)
                return res.json(Utility.output(Toster.USER_BLOCKED, 'ERROR'));
            if (user.status == 3)
                return res.json(Utility.output(Toster.ACCOUNT_REMOVE, 'ERROR'));

            var newVcode = user.generateVcode(new Date().getTime());
            UserModel.update(
                { _id: user._id },
                { $set: { 'v_code': newVcode } }, function (err, numberUpdate) {
                    if (err)
                        return res.json(Utility.output(err, 'ERROR'));
                    if (numberUpdate) {
                        //-------------------------------------------------
                        var data = {
                            company_name: SITE_SETTINGS.COMPANY_NAME,
                            site_name: SITE_SETTINGS.SITE_NAME,
                            contact_person_name: user.name,
                            verify_account: constants.base_url + '#/verification/' + encodeURI(newVcode),
                            join_yuvitime: constants.base_url + '#/login',
                            subscribe_yuvitime: constants.base_url

                        };
                        template_instance.load_template('RESEND_EMAIL_VERIFICATION', data, function (error, html) {
                            if (error) {
                                return res.json(Utility.output(error, 'ERROR'));
                            }
                            var opta = {
                                'to': user.email,
                                'sub': SITE_SETTINGS.SITE_NAME + Toster.EMAIL_VERIFICATION,
                                'body': html
                            };
                            Utility.sendMail(opta, function (err, info) {
                                if (err) {
                                    return res.json(Utility.output(Toster.SEND_MAIL_ERROR, 'ERROR'));
                                } else {
                                    return res.json(Utility.output('Verification mail sent to ' + user.email + ' email address', 'SUCCESS'));
                                }
                            });
                        });
                        //------------------------------------------------------
                        //                    var opta = {
                        //                        'to': user.email,
                        //                        'sub': SITE_SETTINGS.SITE_NAME + " Account email verification mail",
                        //                        'body': html
                        //                    };
                        //                    Utility.sendMail(opta, function (err, info) {
                        //                        if (err) {
                        //                            return res.json(Utility.output('Error in send verification email', 'ERROR'));
                        //                        } else
                        //                        {
                        //                            return res.json(Utility.output('Verification mail sent to "' + user.email + '" email address', 'SUCCESS'));
                        //                        }
                        //                    });
                    }
                });
        });
    };


    /**
    * <h1>[API] Set New Password</h1>
    * <p>This api use for set new password for [Forget Password]</p>
    * @see API_BASE_URL+user/set_new_password</a>
    * @param {String} password_request_token [*Mandatory]
    * @param {String} password [*Mandatory]
    * @header Content-Type: application/x-www-form-urlencoded
    * <pre>
    * {@code
    *    {
    *      "message": "Password has been changed",
    *      "status": "success",
    *      "result": ""
    *    }
    *}
    * </pre>
    * @author Subrata nandi
    */
    this.set_new_password = function (req, res, next) {
        // req.assert('password_request_token', Toster.PASSWORD_TOKEN).notEmpty();
        // req.assert('password', Toster.PASSWORD_REQUIRED).notEmpty(); // Adding validation rules over password
        // req.assert('password', Toster.PASSWORD_VALIDATE).isLength({ min: 6, max: 20 });
        // var errors = req.validationErrors();
        // if (errors) {
        //     var messages = [];
        //     errors.forEach(function (error) {
        //         messages.push(error.msg);
        //     });
        //     return res.json(Utility.output(messages, 'ERROR'));
        // }
        // var decodedContain = new NodeRSA(constants.getPrivateKey()).decrypt(req.body.password_request_token, 'utf8');
        // var extractToken = JSON.parse(decodedContain);
        // if (!extractToken)
        //     return res.json(Utility.output(Toster.INVALID_REQUEST_TOKEN, 'ERROR'));
        // if (extractToken.expire_on < new Date().getTime())
        //     return res.json(Utility.output(Toster.REQUEST_EXPIRED, 'ERROR'));

        // UserModel.findOne({ _id: extractToken.user_id }, function (err, user) {
        UserModel.findOne({ email: req.body.email }, function (err, user) {
            if (err)
                return res.json(Utility.output(err, 'ERROR'));
            if (!user)
                return res.json(Utility.output(Toster.USER_NOT_FOUND, 'ERROR'));
            // if (user.forget_password_request != req.body.password_request_token)
            //     return res.json(Utility.output(Toster.INVALID_REQUEST_TOKEN, 'ERROR'));

            UserModel.update(
                { _id: user._id },
                { $set: { 'forget_password_request': null, 'password': user.encryptPassword(req.body.password) } }, function (err, numberUpdate) {
                    if (err)
                        return res.json(Utility.output(err, 'ERROR'));
                    if (numberUpdate)
                        return res.json(Utility.output(Toster.PASSWORD_CHANGED, 'SUCCESS'));
                });
        });
    };


    /**
    * <h1>[Member Function] IsLogin</h1>
    * <p>This function use check that Authentication token header is valid or not
    * This is a Middleware function</p>
    * @author Subrata nandi
    */
    this.isLoggedIn = function (req, res, next) {
        var requestParser = require('ua-parser').parse(req.headers['user-agent']);
        var headers = {
            'Authorization': req.headers.authorization,
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        var options = {
            'url': constants.auth_server + '/check_login',
            'method': 'POST',
            'headers': headers,
            'form': {
                'device': requestParser.device.family,
                'browser': requestParser.ua.toString(),
                'os': requestParser.os.toString(),
                'ip': (req.ip == '::1') ? 'localhost' : req.ip,
            }
        };

        request(options, function (error, response, body) {
            if (!error && response.statusCode == 200) {
                body = JSON.parse(body);
                if (body.status == "error")
                    return res.json(Utility.output(body.message, 'ERROR', "", { 'logout': true }));
                UserModel.findOne({ _id: body.result }, function (err, user) {
                    if (err)
                        res.json(Utility.output(err, 'ERROR'));
                    if (!user)
                        res.json(Utility.output(Toster.USER_NOT_FOUND, 'ERROR'));
                    req.user = user;
                    next();
                });
            } else
                return res.json(Utility.output(Toster.SOMETHING_WRONG, 'ERROR'));
        });
    };



     

    /**
    * <h1>[API] Get users to Edit </h1>
    * <p>This api use for getting information to edit</p>
    * @see API_BASE_URL+get_by_id/</a>
    * @param {String}  [*Mandatory]
    * @header Content-Type: application/x-www-form-urlencoded
    * <pre>
    * {@code
    *{
    *    "message": "get employee details successfully",
    *    "status": "success",
    *    "result": ""
    *  }
    *}
    * </pre>
    * @author Subrata Nandi 
    */
    this.get_user_by_id = function (req, res, next) {
        var currentTime = new Date().getTime();
        UserModel.findOne({ email: req.body.email }, function (err, user) {
            if (err) {
                return res.json(Utility.output(err, 'ERROR'));
            }
            if (user) {
                return res.json(Utility.output(Toster.GET_EPLOYEE_BY_ID, 'SUCCESS', user));
            }
        });
    };

    /**
      * <h1>[API] Edit Employee </h1>
      * <p>This api use for editing an employee</p>
      * @see API_BASE_URL+edit_employee/</a>
      * @param {String}  [*Mandatory]
      * @header Content-Type: application/x-www-form-urlencoded
      * <pre>
      * {@code
      *{
      *    "message": "edit employee successfull",
      *    "status": "success",
      *    "result": ""
      *  }
      *}
      * </pre>
      * @author Subrata Nandi 
      */
    this.edit_user_by_id = function (req, res, next) {
        var headers = {
            'Authorization': req.headers.authorization,
            'Content-Type': 'application/x-www-form-urlencoded'
        };

        var currentTime = new Date().getTime();
        req.assert('fullname', Toster.NAME_REQUIRED).notEmpty();
        req.assert('fullname', Toster.NAME_MINIMUM).optional().isLength({ min: 3, max: 255 });
        req.assert('user_name', Toster.NAME_REQUIRED).notEmpty();
        req.assert('user_name', Toster.NAME_MINIMUM).optional().isLength({ min: 3, max: 255 });
        req.assert('email', Toster.EMAIL_REQUIRED).notEmpty();
        req.assert('email', Toster.VALID_EMAIL).isEmail();
        req.assert('password', Toster.PASSWORD_REQUIRED).notEmpty();
        req.assert('password', Toster.PASSWORD_VALIDATE).optional().isLength({ min: 6, max: 20 });
        req.assert('skills', 'Skill is required').notEmpty();
        req.assert('address', Toster.ADDRESS_VALIDATE).optional().isLength({ min: 15, max: 100 });
        req.assert('designation', 'Designation is required').notEmpty();
        req.assert('mobile', Toster.MOBILE_VALIDATE).optional().isLength({ min: 10, max: 20 });

        var errors = req.validationErrors();
        if (errors) {
            var messages = [];
            errors.forEach(function (error) {
                messages.push(error.msg);
            });
            return res.json(Utility.output(messages, 'ERROR'));
        }
        UserModel.findOne({ email: req.body.email }, function (err, user) {
            if (err) {
                return res.json(Utility.output(err, 'ERROR'));
            }
            if (user) {
                var newUser = {};
                newUser.fullname = Utility.escape(req.body.fullname);
                newUser.user_name = Utility.escape(req.body.user_name);
                newUser.email = req.body.email.toLowerCase().toLowerCase();
                newUser.status = 1;
                newUser.mobile = req.body.mobile;
                newUser.address = req.body.address;
                newUser.skills = req.body.skills;
                newUser.designation = req.body.designation;
                newUser.date_of_creation = currentTime;
                newUser.date_of_modification = currentTime;
                newUser.joining_date = currentTime;
                UserModel.update({ email: req.body.email }, { $set: newUser }).exec(function (err, result) {
                    if (err) {
                        return res.json(Utility.output(err, 'ERROR'));
                    }
                    return res.json(Utility.output(Toster.EDIT_EPLOYEE_BY_ID, 'SUCCESS', result));
                    // return res.status(200).json(Utility.output(Toster.REGISTRATION_COMPLETE, 'SUCCESS',result));
                });
            }
            // if (isNewUser)
            // {
            //   UserType.findOne({slug: 'end-user'}, function (err, userType) {
            //         var newUser = new UserModel();
            //         newUser.fullname = Utility.escape(req.body.fullname);
            //         newUser.user_name = Utility.escape(req.body.user_name);
            //         newUser.email = req.body.email.toLowerCase().toLowerCase();
            //         newUser.status = 1;
            //         newUser.image = "";
            //         newUser.password = newUser.encryptPassword(req.body.password); 
            //         newUser.v_code = newUser.generateVcode(currentTime); 
            //         newUser.mobile = req.body.mobile;
            //         newUser.address = req.body.address;
            //         newUser.skills = req.body.skills;
            //         newUser.designation = req.body.designation;
            //         newUser._user_type = userType;
            //         newUser.joining_date = req.body.joining_date;
            //         newUser.date_of_creation = currentTime;
            //         newUser.date_of_modification = currentTime;
            //         newUser.joining_date = currentTime;

            //         newUser.save(function (err, result) {
            //             if (err) {
            //                 return res.json(Utility.output(err, 'ERROR'));
            //             }
            //            return res.status(200).json(Utility.output(Toster.REGISTRATION_COMPLETE, 'SUCCESS',result));
            //          });
            //     });
            // }
        });
    };

    this.send_mail = function (req, res, next) {
         var currentTime = new Date().getTime();
                    var contactUser = new ContactModel();
                    contactUser.fullName = req.body.fullName;
                    contactUser.email = req.body.email.toLowerCase().toLowerCase();
                    contactUser.status = 1;
                    contactUser.mobile = Utility.escape(req.body.mobile);
                    contactUser.msg = req.body.msg;
                    contactUser.date_of_creation = currentTime;
                    contactUser.date_of_modification = currentTime;
                    contactUser.save(function (err, result) {
                    if (err){
                             res.status(226).send({ status: 0, msg:'Unable to save data' });
                            }else{
                               // go to https://myaccount.google.com/lesssecureapps?pli=1
                              //  Less secure app access
                             //   Allow less secure apps: ON
                               var transporter = nodemailer.createTransport({
                                service: 'gmail',
                                auth: {
                                user: 'subrata.bizzman@gmail.com',//replace with your email
                                pass: '@bizz2019'//replace with your password
                                }
                                });
                                /*
                                In mailOptions we specify from and to address, subject and HTML content.
                                In our case , we use our personal email as from and to address,
                                Subject is Contact name and
                                html is our form details which we parsed using bodyParser.
                                */
                                var mailOptions = {
                                from: 'saswati.ushasi@gmail.com',//replace with your email
                                to: 'subrata.ushasi@gmail.com',//replace with your email
                                subject: `Contact name: ${req.body.fullName}`,
                                html:`<h1>Contact details</h1>
                                <h2> name:${req.body.fullName} </h2><br>
                                <h2> email:${req.body.email} </h2><br>
                                <h2> phonenumber:${req.body.mobile} </h2><br>
                                <h2> message:${req.body.msg} </h2><br>`
                                };
                                /*
                                Here comes the important part, sendMail is the method which actually sends email, it takes mail options and
                                call back as parameter
                                */
                                transporter.sendMail(mailOptions, function(error, info){
                                if (error) {
                                console.log(error);
                                res.send('error') // if error occurs send error as response to client
                                }
                                else {
                                console.log('Email sent: ' + info.response);
                                res.status(200).send(info);
                                    }
                                });
                               


                        //             let mailOptions = {
                        //                 to: req.body.email.toLowerCase(),
                        //                 subject: req.body.msg,
                        //                 html: `
                        //                     <html xmlns="http://www.w3.org/1999/xhtml">

                        //                     <head>

                        //                       <!-- If you delete this meta tag, Half Life 3 will never be released. -->
                        //                       <meta name="viewport" content="width=device-width">
                        //                       <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
                        //                       <title>Cv-maker Emailer</title>
                        //                     </head>

                        //                     <body>
                        //                       <table class="wrapper" bgcolor="#ECEEF1" style="background-color:#ECEEF1;width:100%;">
                        //                         <tr>
                        //                           <td>
                        //                             <!-- HEADER -->
                        //                             <table class="head-wrap" style="margin: 0 auto;">
                        //                               <tr>
                        //                                 <td></td>
                        //                                 <td class="header container logo" style="font-family:Source Sans Pro, Helvetica, sans-serif;color:#6f6f6f;display:block;margin:0 auto;max-width:600px;padding:20px">
                        //                                   <div class="content logo" style="display:block;margin:0 auto;max-width:650px;-webkit-font-smoothing:antialiased;padding:20px">
                        //                                     <!--<table>
                        //                                       <tr>
                        //                                         <td>

                        //                                         </td>
                        //                                       </tr>
                        //                                     </table>-->
                        //                                   </div>
                        //                                 </td>
                        //                                 <td></td>
                        //                               </tr>
                        //                             </table>
                        //                             <!-- /HEADER -->
                        //                             <!-- BODY -->
                        //                             <table class="body-wrap" style="margin:0 auto;margin-bottom: 50px;">
                        //                               <tr>
                        //                                 <td></td>
                        //                                 <td class="container transaction-mailer" bgcolor="#FFFFFF" style="font-family:Source Sans Pro, Helvetica, sans-serif;color:#6f6f6f;display:block;max-width:600px;margin:0 auto;padding:40px;border:1px solid #CED3D8;">
                        //                                   <div class="content" style="display:block;margin:0 auto;max-width:650px;padding:20px;-webkit-font-smoothing:antialiased">
                        //                                     <div class="receipt">
                        //                                       <div class="divider">
                        //                                         <div class="message">
                        //                     <img alt="our wonderful wistia logo" border="0" class="wistia-logo" height="46" src="images/bizzman-logo.png" width="100" style="display:block">
                        //                                             <br>
                        //                                           <h1 class="emphasis" style="margin:0;padding:0;margin-bottom:20px;font-weight:700;margin-top:10px;-webkit-font-smoothing:antialiased;font-size:28px;line-height:130%;text-align:left;color:#54bbff">Thank you for subscribing to CVMaker
                        //                     .</h1>

                        //                                           <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">To complete your profile and start creating your distinctive CV, please click the following button.

                        //                                           <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">Want to know more? Reach us at <a href="#" target="_blank" style="color:#54bbff">address link</a></p>
                        //                                           <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">
                        //                                             <br>
                        //                                              <a href=`+req.body.email+` style="color:#54bbff; background-color: #f80b82; padding: 10px 25px; color: #fff; text-decoration: none; border-radius: 4px;">Confirm My Email Address</a>
                        //                                           </p>
                        //                                         </div>
                        //                                       </div>

                        //                                       <div class="foot">
                        //                                         <p style="color:#434343;text-align:left;line-height:150%;padding:0;font-weight:400;font-size:18px">
                        //                                           <strong>Thanks.</strong> 
                        //                                           <h3 style="text-align: center; ">FOLLOW US ON </h3>
                        //                                                              <div style="text-align: center;">
                        //                                                                  <a href="https://www.facebook.com/Bizzmankolkata/?ref=br_rs">    <img src="images/unnamed.png" class="img-responsive" style="margin-right: 20px;" /></a>
                        //                                                                  <a href="https://twitter.com/BizzmanW"> <img src="images/twitter.png" class="img-responsive" style="margin-right: 20px;" /> </a>
                        //                                                                  <a href="https://www.linkedin.com/company/bizzman-design-marketing/">
                        //                                                                  <img src="images/insta.png">
                        //                                                                  </a>
                        //                                                             </div>
                        //                                                             <p style="text-align: center; margin-top: -10px;">Partner With BizzmanWeb<span style="color: #f76c6c; font-size: 38px;">♥</span></p>
                        //                                                             <p style="text-align: center;">EN-62, Sector V, Salt Lake City, Kolkata, West Bengal 700091</p>
                        //                                                             <p style="text-align: center;">Get in touch at <span style="color: #00b0bb">info@bizzmanweb.com</span> or<span style="color: #00b0bb;"> +918637360492 /+9184200442189 </span>.
                        //                                             </p>
                        //                                           </p>
                        //                                       </div>
                        //                                     </div>
                        //                                   </div>
                        //                                   <!-- /content -->
                        //                                 </td>
                        //                                 <td></td>
                        //                               </tr>
                        //                               <tr>
                        //                                 <td></td>
                        //                                 <td></td>
                        //                               </tr>
                        //                             </table>
                        //                             <!-- /BODY -->
                        //                           </td>
                        //                         </tr>
                        //                       </table>
                        //                       <!-- /wrapper -->

                        //                     </body>

                        //                     </html>` 

                        //             }

                        //             smtpTransport.sendMail(mailOptions, function (error, response) {
                        //                 if (error) {
                        //                     console.log(error);
                        //                     res.end("error");
                        //                 } else {
                        //                     // console.log(response.response);
                        //                     // res.end("sent");
                        //                     res.status(200).send(result);
                        //                 }
                        // });
                }
        });
    }
};