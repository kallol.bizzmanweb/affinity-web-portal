var UserModel = require('../models/users');
var UserType = require('../models/user_types');
var UserLogModel = require('../models/user_log');
var MyinfoLoginModel = require('../models/myinfo_logins');
var SaveMyinfoModel = require('../models/save_myinfos');
var NodeRSA = require('node-rsa');
var request = require('request');
var jwt = require('jsonwebtoken');
var nodemailer = require("nodemailer");
var multer = require('multer');
// var EmailController = require('./EmailTemplateController');
// var template_instance = new EmailController();
module.exports = function MyinfoController() {

    /** 
     * <h1>[API] Template initialization</h1>
     * <p>This api use for Template initilization[Initialization] by template id</p>
     * @see API_BASE_URL+templates/init</a>
     * @param {String} email [*Mandatory][Valid Email]
     * @param {String} templatid [*Mandatory]
     * @param {String} last_activity [Optional]
     * @param {String} status [Optional]
     * @header Content-Type: application/x-www-form-urlencoded
     * <pre>
     * {@code
     *    {
     *      "message": "Template initialized successfully",
     *      "status": "success",
     *      "result": ""
     *    }
     * </pre>
     * @description after login left nav bar sub menu option
     * @author Subrata Nandi
     */
    
    this.upgrade_pro = function(req,res,next){
        var currentTime = new Date().getTime();
        UpgradeFeatureModel.findOne({category:req.body.category},function(err,upgradeInfo){
            if(err){
                 return res.json(Utility.output(err, 'ERROR'));
            }
            return res.json(Utility.output(Toster.EDIT_EPLOYEE_BY_ID, 'SUCCESS', upgradeInfo));
        });
    };

    this.getPersonData = function(req,res,next){
        var currentTime = new Date().getTime();
        MyinfoLoginModel.findOne({ email : req.body.email }, function (err, myinfoData) {
            if (err){
                return res.json(Utility.output(Toster.FETCH_INFO_ERROR, 'ERROR'));
                }
                return res.json(Utility.output(Toster.EDIT_EPLOYEE_BY_ID, 'SUCCESS', myinfoData));
            });
        };


    this.postPersonData = function(req,res,next){
            var currentTime = new Date().getTime();
            var myinfoData = new SaveMyinfoModel();
                //  var createResume = new CreateResumeModel();
                myinfoData.uinfin = req.body.uinfin;
                myinfoData.name  = req.body.name; 
                myinfoData.sex   = req.body.sex;
                myinfoData.race    = req.body.race;
                myinfoData.nationality  = req.body.nationality;
                myinfoData.dob  = req.body.dob;
                myinfoData.email  = req.body.email;
                myinfoData.mobileno   = req.body.mobileno;
                myinfoData.regadd = req.body.regadd;
                myinfoData.date_of_creation = currentTime;
                myinfoData.date_of_modification = currentTime;
                myinfoData.save(function(err,result){
                     if (err) {
                        return res.json(Utility.output(err, 'ERROR'));
                    }
                    return res.status(200).json(Utility.output(Toster.REGISTRATION_COMPLETE, 'SUCCESS',result));
                });
                // CreateResumeModel.update({ email: req.body.email,resume_id:req.body.resume_id }, { $set: createResume }).exec(function (err, result) {
                //     if (err) {
                //         return res.json(Utility.output(err, 'ERROR'));
                //     }
                //     return res.json(Utility.output(Toster.EDIT_EPLOYEE_BY_ID, 'SUCCESS', result));
                    // return res.status(200).json(Utility.output(Toster.REGISTRATION_COMPLETE, 'SUCCESS',result));
             //   });
        };
};