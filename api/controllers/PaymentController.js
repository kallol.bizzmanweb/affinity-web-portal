var UserModel = require('../models/users');
var UserType = require('../models/user_types');
var UserLogModel = require('../models/user_log');
var PaymentsModel = require('../models/payments');
var PriceModel = require('../models/prices');
var NodeRSA = require('node-rsa');
var request = require('request');
var jwt = require('jsonwebtoken');
var nodemailer = require("nodemailer");
var multer = require('multer');
// var EmailController = require('./EmailTemplateController');
// var template_instance = new EmailController();
module.exports = function PaymentController() {

    /** 
     * <h1>[API] Template initialization</h1>
     * <p>This api use for Template initilization[Initialization] by template id</p>
     * @see API_BASE_URL+templates/init</a>
     * @param {String} email [*Mandatory][Valid Email]
     * @param {String} templatid [*Mandatory]
     * @param {String} last_activity [Optional]
     * @param {String} status [Optional]
     * @header Content-Type: application/x-www-form-urlencoded
     * <pre>
     * {@code
     *    {
     *      "message": "Template initialized successfully",
     *      "status": "success",
     *      "result": ""
     *    }
     * </pre>
     * @description after login left nav bar sub menu option
     * @author Subrata Nandi
     */
    
    // start payment process 
this.buy = function ( req , res ,next )  {
	// create payment object 
    var payment = {
            "intent": "authorize",
	"payer": {
		"payment_method": "paypal"
	},
	"redirect_urls": {
		"return_url": "http://127.0.0.1:3000/success",
		"cancel_url": "http://127.0.0.1:3000/err"
	},
	"transactions": [{
		"amount": {
			"total": 39.00,
			"currency": "USD"
		},
		"description": " a book on mean stack "
	}]
    };
	
	
	// call the create Pay method 
    createPay( payment ) 
        .then( ( transaction ) => {
            var id = transaction.id; 
            var links = transaction.links;
            var counter = links.length; 
            while( counter -- ) {
                if ( links[counter].method == 'REDIRECT') {
					// redirect to paypal where user approves the transaction 
                    return res.redirect( links[counter].href );
                }
            }
        })
        .catch( ( err ) => { 
            console.log( err ); 
            res.redirect('/err');
        });
    }; 

    // success page 
    this.success = function (req ,res ) {
            console.log(req.query); 
           
        };
    // error page 
    this.err = function (req ,res ) {
            console.log(req.query); 
           
        };


    var createPay = ( payment ) => {
            return new Promise( ( resolve , reject ) => {
                paypal.payment.create( payment , function( err , payment ) {
                if ( err ) {
                    reject(err); 
                }
                else {
                    resolve(payment); 
                }
                }); 
            });
        }	


    this.cancel_payment = function(req,res,next){
        var currentTime = new Date().getTime();
        PriceModel.findOne({ item_id : req.body.item_id }, function (err, priceInfo) {
            if (err){
                return res.json(Utility.output(Toster.FETCH_INFO_ERROR, 'ERROR'));
                }
                return res.json(Utility.output(Toster.EDIT_EPLOYEE_BY_ID, 'SUCCESS', priceInfo));
            });
        };


    this.update_resume = function(req,res,next){
            var currentTime = new Date().getTime();
            var createResume = {};
                //  var createResume = new CreateResumeModel();
                createResume.fullname = req.body.fullname;
                createResume.address  = req.body.address; 
                createResume.mobile   = req.body.mobile;
                createResume.email    = req.body.email;
                createResume.user_id  = req.body.user_id;
                createResume.resume_id  = req.body.resume_id;
                createResume.summery  = req.body.summery;
                createResume.skills   = req.body.skills;
                createResume.text_section = req.body.text_section;
                createResume.portfolio = req.body.portfolio;
                createResume.custom_date = {
                                          start_date:req.body.start_date,
                                          end_date:req.body.end_date
                                         };
                createResume.charts  = req.body.charts;
                createResume.backgound = req.body.backgound;
                CreateResumeModel.update({ email: req.body.email,resume_id:req.body.resume_id }, { $set: createResume }).exec(function (err, result) {
                    if (err) {
                        return res.json(Utility.output(err, 'ERROR'));
                    }
                    return res.json(Utility.output(Toster.EDIT_EPLOYEE_BY_ID, 'SUCCESS', result));
                    // return res.status(200).json(Utility.output(Toster.REGISTRATION_COMPLETE, 'SUCCESS',result));
                });
        };
};