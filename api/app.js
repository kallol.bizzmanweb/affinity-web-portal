var createError = require('http-errors');
var express = require('express');
global.Toster = require('./toster/toster_messages');
global.Utility=require('./lib/utility');
global.constants = require('./config/constants');
require('./config/site_settings');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var bodyParser = require('body-parser');
var expressHbs = require('express-handlebars');
global.mongoose = require('mongoose');
mongoose.Promise = require('bluebird');
global.ObjectID=require('mongoose').Types.ObjectId;
var session = require('express-session');
var passport = require('passport');
global.jwt = require('jsonwebtoken');
var flash = require('connect-flash');
//var validator = require('express-validator');
var MongoStore = require('connect-mongo')(session);
var indexRouter = require('./routes/index');
var usersRouter = require('./routes/user');


var app = express();
//app.use(compression());
// var promise = mongoose.connect('mongodb://103.14.97.154/cv-maker',{
 //var promise = mongoose.connect('mongodb://admin:admin@103.14.97.154:27017/admin',{
 var promise = mongoose.connect('mongodb://localhost/afs',{
   useUnifiedTopology: true ,
   useNewUrlParser: true
});
promise.then(function(db){
 console.log('Mongo running successful');
});
// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'hbs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));

//app.use(validator());

app.use(cookieParser());
app.use(passport.initialize());
app.use(passport.session());
app.use(express.static(path.join(__dirname, 'public')));


app.use(function(req,res,next){
  /*
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');
  */
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header("Access-Control-Allow-Headers", "Content-Type, Authorization");
  res.header("Access-Control-Allow-Credentials", true);

  next();
});

app.use('/', indexRouter);
app.use('/api', usersRouter);




// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
